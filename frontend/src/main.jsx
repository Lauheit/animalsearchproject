import React from 'react'
import ReactDOM from 'react-dom/client'
import Router from './componetns/ui/Router.jsx'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <Router />
  </React.StrictMode>,
)
