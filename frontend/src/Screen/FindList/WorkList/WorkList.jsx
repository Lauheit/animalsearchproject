import Footer from '../../Auth/components/Footer'
import Header from '../../Offer/components/Header'
import WorkList_block from './components/WorkList_block'

const WorkList = () => {
    return(
        <>
        <Header />
        <WorkList_block />
        <Footer />
        </>
    )
}

export default WorkList