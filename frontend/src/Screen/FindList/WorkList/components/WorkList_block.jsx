import { useState, useEffect } from "react";
import styles from '../../components/modules/FindList.module.css';
import { Container, Row, Col } from "react-bootstrap";
import CustomModalWork from "./ModalWork";

const WorkList_block = () => {
    const [list, setList] = useState([])
    const [isOpen, setIsOpen] = useState(false);
    const [selectedItem, setSelectedItem] = useState(null);
    const link = '/api/applications/get_user_app'
    const openModal = (item) => {
      setSelectedItem(item);
      setIsOpen(true);
  };

  const closeModal = () => {
      setIsOpen(false);
      setSelectedItem(null);
  };
  
    useEffect(() => {
        const fetchData = async () => {
          const jsonData = {
            status: "В работе"
          }
          const access_token = localStorage.getItem('access_token');
        try {const headers = {
            Authorization: `Bearer ${access_token}`,
            'Content-Type': 'application/json'
          };
        
          const requestOptions = {
            method: 'POST',
            headers: headers,
            body: JSON.stringify(jsonData),
          };
        
          const response = await fetch(link, requestOptions);
            const data = await response.json();
            setList(data);
            console.log(data)
        } catch (error) {
            console.log(error);
        }
      };
        fetchData();
      }, []);
    return(
        <section className={styles.findlist}>
      <Container>
        <Row>
          <h1 className={styles.title}>В работе</h1>
        </Row>
        <Row className={styles.petmenu}>
          {list.length ? list.map((item) => (   
            <Col xxl="3" className={`text-center ${styles.petitem}`} key={item.id}>
              <div className={`text-center ${styles.padding}`}>
                <div onClick={() => openModal(item)} className={ `nav-link ${styles.name}`}>{item.animal_name}</div>
              </div>
            </Col>
          )):<p>Заявок в данный момент нет</p>}
        </Row>
        {isOpen && (
        <CustomModalWork  isOpen={isOpen} closeModal={closeModal} selectedItem={selectedItem} />
      )}
      </Container>
    </section>
    )
}

export default WorkList_block