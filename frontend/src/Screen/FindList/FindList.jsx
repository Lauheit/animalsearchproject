import FindList_block from './components/FindList_block'
import Header from '../Offer/components/Header'
import Footer from '../Auth/components/Footer'

const FindList = () =>{
    return(
        <>
            <Header />
            <FindList_block />
            <Footer />
        </>
    )
}

export default FindList