import { useState, useEffect } from "react";
import FilterMenu from "./modules/droplist";
import styles from "./modules/FindList.module.css";
import { Container, Row, Col, Button } from "react-bootstrap";
import CustomModal from "./Modal";

const FindList_block = () => {
    const link = '/api/applications/get_all_app'
    const [list, setList] = useState([]);
    const [isOpen, setIsOpen] = useState(false);
    const [selectedItem, setSelectedItem] = useState(null);

    const openModal = (item) => {
        setSelectedItem(item);
        setIsOpen(true);
    };

    const closeModal = () => {
        setIsOpen(false);
        setSelectedItem(null);
    };
    useEffect(() => {
      const fetchData = async () => {
      try {
          const response = await fetch(link);
          const data = await response.json();
          setList(data);
          console.log(data)
      } catch (error) {
          console.log(error);
      }
    };
      fetchData();
    }, []);

    return (
    <section className={styles.findlist}>
      <Container>
        <Row>
          <h1 className={styles.title}>Объявления</h1>
        </Row>
        <FilterMenu />
        <Row className={styles.petmenu}>
          {list.length ? list.map((item) => (
            <Col xxl="3" className={`text-center ${styles.petitem}`} key={item.id}>
              <div className={`text-center ${styles.padding}`}>
                <img
                  src={item.photo}
                  alt=""
                  className={`w-100 ${styles.image}`}
                />
                <h1 className={`${styles.name}`}>{item.animal_name}</h1>
                <p className={styles.data}>Пропал 4.07.23</p>
                <Button className="" onClick={() => openModal(item)}>
                  Подробнее
                </Button>
              </div>
            </Col>
          )):<p>Заявок в данный момент нет</p>}
        </Row>
        {isOpen && (
        <CustomModal  isOpen={isOpen} closeModal={closeModal} selectedItem={selectedItem} />
      )}
        <Row className="justify-content-center">
          <Col xxl="2">
            <div className={`btn btn-custom w-100 ${styles.btntext}`}>
              Показать еще
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default FindList_block;
