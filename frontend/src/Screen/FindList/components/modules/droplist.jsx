import React, { useState } from "react";
import { Col, Row } from "react-bootstrap";
import Dropdown from "react-bootstrap/Dropdown";
import styles from './FindList.module.css'


const FilterMenu = () => {
  const [selectedFilters, setSelectedFilters] = useState([]);

  const filters = [
    { value: "Filter 1", label: "За эту неделю", key: "filter1" },
    { value: "Filter 2", label: "Собаки", key: "filter2" },
    { value: "Filter 3", label: "Чарли", key: "filter3" },
  ];

  const handleFilterSelect = (filter) => {
    if (selectedFilters.some((selectedFilter) => selectedFilter.key === filter.key)) {
      setSelectedFilters(selectedFilters.filter((selectedFilter) => selectedFilter.key !== filter.key));
    } else {
      setSelectedFilters([...selectedFilters, filter]);
    }
  };

  const handleRemoveFilter = (key) => {
    setSelectedFilters(selectedFilters.filter((filter) => filter.key !== key));
  };

  return (
    <>
    <Row>
      <Col xxl="2">
        <Dropdown className={`${styles.margin}`}>
          <Dropdown.Toggle className={`w-100 ${styles.btnhovertoggle} ${styles.btncustom}`} variant="none" id="dropdown-filter">
            Фильтры
          </Dropdown.Toggle>
          <Dropdown.Menu>
            {filters.map((filter) => (
              <Dropdown.Item
                key={filter.key}
                onClick={() => handleFilterSelect(filter)}
                active={selectedFilters.some((selectedFilter) => selectedFilter.key === filter.key)}
              >
                {filter.label}
              </Dropdown.Item>
            ))}
          </Dropdown.Menu>
        </Dropdown>
      </Col>
    </Row>
      {selectedFilters.length > 0 && (
        <Row>
          {selectedFilters.map((filter) => (
            <Col xxl="2" key={filter.key}>
              <div className={`btn ${styles.btncustom} ${styles.btncustompd} w-100`} onClick={() => handleRemoveFilter(filter.key)}>
                {filter.label}
              </div>
            </Col>
          ))} 
        </Row>
      )}
    </>
  );
};

export default FilterMenu;
