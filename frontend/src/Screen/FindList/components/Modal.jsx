import { useState } from "react";
import { Modal, Button, FormGroup, FormLabel, FormText } from "react-bootstrap";
import { useNavigate } from "react-router";

const CustomModal = ({ isOpen, closeModal, selectedItem }) => {
  const navigate = useNavigate()
  const linkWork = '/api/applications/edit'
  const access_token = localStorage.getItem('access_token');
  const [error, setError] = useState('')
  const handleClickWork = async () => {
    const jsonData = {
      id_app: selectedItem.id,
      status: "В работе"
    }
    try {
      const headers = {
        Authorization: `Bearer ${access_token}`,
        'Content-Type': 'application/json'
      };
    
      const requestOptions = {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(jsonData),
      };
    
      const response = await fetch(linkWork, requestOptions);
    
      const responseData = await response.json();
    
      if (responseData.hasOwnProperty('error')) {
        setError(responseData.error);
        console.log(responseData.error);
      } else {
        const successResponse = responseData;
        console.log(successResponse);
        closeModal();
        navigate('/worklist')

      }
    } catch (error) {
      setError("Произошла ошибка при отправке формы.");
      console.error(error);
    }

  }
  return (
    <Modal show={isOpen} onHide={closeModal}>
      <Modal.Header closeButton>
        <Modal.Title>Имя питомца {selectedItem.animal_name}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <FormGroup>
            <FormLabel>Статус заявки</FormLabel>
            <FormText> {selectedItem.status}</FormText>
        </FormGroup>
        <FormGroup>
            <FormLabel>Тип питомца</FormLabel>
            <FormText> {selectedItem.animal}</FormText>
        </FormGroup>
        <FormGroup>
            <FormLabel>Описание</FormLabel>
            <FormText> {selectedItem.description}</FormText>
        </FormGroup>
        <FormGroup>
            <FormLabel>Как пропал</FormLabel>
            <FormText> {selectedItem.circ}</FormText>
        </FormGroup>
        {/* Дополнительная информация об объявлении */}
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClickWork}>
          В работу
        </Button>
      </Modal.Footer>
    </Modal>
  );
};
export default CustomModal