import { Modal, Button, FormGroup, FormLabel, FormText } from "react-bootstrap";


const CustomModalClose = ({isOpen, closeModal, selectedItem}) => {
    return (
        <Modal show={isOpen} onHide={closeModal}>
          <Modal.Header closeButton>
            <Modal.Title>Статус заявки {selectedItem.status}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <FormGroup>
                    <FormLabel>Имя питомца</FormLabel>
                    <FormText> {selectedItem.animal}</FormText>
                </FormGroup>
                <FormGroup>
                    <FormLabel>Тип питомца</FormLabel>
                    <FormText> {selectedItem.animal_name}</FormText>
                </FormGroup>
                <FormGroup>
                    <FormLabel>Описание</FormLabel>
                    <FormText> {selectedItem.description}</FormText>
                </FormGroup>
                <FormGroup>
                    <FormLabel>Как пропал</FormLabel>
                    <FormText> {selectedItem.circ}</FormText>
                </FormGroup>
                    {/* Дополнительная информация об объявлении */}
            </Modal.Body>
            <Modal.Footer>
            <Button variant="secondary" onClick={closeModal}>
              Закрыть окно
            </Button>
          </Modal.Footer>
        </Modal>
      );
}

export default CustomModalClose