import { useRef, useState, useEffect } from 'react';
import { Navigation,} from 'swiper/modules';
import { Swiper, SwiperSlide } from "swiper/react";
import CustomModalClose from './CustomModalClose';

const SwiperPet = ({ onListLengthChange }) => {
    const [list, setList] = useState([])
    const [isOpen, setIsOpen] = useState(false);
    const [selectedItem, setSelectedItem] = useState(null);
    const link = '/api/applications/get_user_app'
    const swiperRef = useRef(null);
    const openModal = (item) => {
        setSelectedItem(item);
        setIsOpen(true);
    };
  
    const closeModal = () => {
        setIsOpen(false);
        setSelectedItem(null);
    };
    useEffect(() => {
        const fetchData = async () => {
          const jsonData = {
            status: "Закрыто"
          }
          const access_token = localStorage.getItem('access_token');
        try {const headers = {
            Authorization: `Bearer ${access_token}`,
            'Content-Type': 'application/json'
          };
        
          const requestOptions = {
            method: 'POST',
            headers: headers,
            body: JSON.stringify(jsonData),
          };
        
          const response = await fetch(link, requestOptions);
            const data = await response.json();
            setList(data);
            console.log(data)
        } catch (error) {
            console.log(error);
        }
      };
        onListLengthChange(list.length);
        fetchData();
      }, [onListLengthChange, list.length]);
    return (
        <>
        <Swiper 
            speed={2000}
            ref={swiperRef}
            navigation={{
            prevEl: ".l-arrow",
            nextEl: ".r-arrow",
            }}
            className="col-xxl-10 offset-xxl-1" 
            modules={[Navigation,]} 
            spaceBetween={24} 
            slidesPerView={4}
        >
            <div className="swiper-wrapper align-items-center">
                <div className="col-xxl-1 offset-xxl-1 align-items-center justify-content-end d-flex">
                    <div className="l-arrow arrow"></div>
                </div>
                {list.length ? list.map((item) => ( 
                <div className="col-xxl-2" key={item.id}>
                    <SwiperSlide >
                        <div className="avatar-block">
                            <img src={item.photo} alt="" className="avatar w-100" />
                        </div>
                        <div className="name-pet">{item.animal_name}</div>
                        <div className="data">Найдем в течении <span className="accent">5</span> часов</div>
                        <div onClick={() => openModal(item)} className="more">Подробнее</div>
                    </SwiperSlide>
                </div>
                 )):<p>Заявок в данный момент нет</p>}
                <div className="col-xxl-1 justify-content-start align-items-center d-flex">
                    <div className="r-arrow arrow"></div>
                </div>
            </div>
        </Swiper>
        {isOpen && (
            <CustomModalClose  isOpen={isOpen} closeModal={closeModal} selectedItem={selectedItem} />
        )}
        </>
    );
}

export default SwiperPet;
