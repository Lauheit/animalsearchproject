import { NavLink } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
const Header = () => {
  const handleLogOut = () => {
    localStorage.removeItem("access_token");
    navigate("/");
  };
  const navigateToFindList = () => {
    navigate("/findlist");
  }
  const navigateToAbout = () => {
    navigate("/#aboutme");
  }
  const navigateToFooter = () => {
    navigate("/#footer");
  }
  const navigateToHelp = () => {
    navigate("/#easysteps");
  }
  const navigateToWorkList = () => {
    navigate("/worklist");
  }
  const navigateToHome = () => {
    navigate("/");
  }
  const navigate = useNavigate();
  return (
    <header className="header" id="header">
      <div className="container">
        <nav className="navbar navbar-expand-lg font-size">
          <div className="col-xxl-3 d-flex">
            <NavLink onClick={navigateToHome} className="navbar-brand">
              <div className="logotip"></div>
            </NavLink>
          </div>
          <div className="col-xxl-8">
            <div
              className="collapse navbar-collapse text-uppercase"
              id="navbarSupportedContent"
            >
              <ul className="navbar-nav">
                <li className="nav-item">
                  <NavLink onClick={navigateToHelp}>
                    Помощь
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink onClick={navigateToAbout}>
                    О нас
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink onClick={navigateToFooter}>
                    Отзывы
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink onClick={navigateToFindList}>
                    Объявления
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink onClick={navigateToWorkList}>
                    Текущие
                  </NavLink>
                </li>
              </ul>
            </div>
          </div>
          <div className="col-xxl-1 text-uppercase text-end">
            <NavLink onClick={handleLogOut}>
              Выйти
            </NavLink>
          </div>
        </nav>
      </div>
    </header>
  );
};

export default Header;
