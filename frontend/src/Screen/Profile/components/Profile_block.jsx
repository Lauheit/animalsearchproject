import { useState, useEffect } from 'react';
import styles from './modules/Profile.module.css'
import { useNavigate } from 'react-router';
import SwiperSlidePet from './modules/SwiperSlidePet';

const Profile_block = () =>{
    const navigate = useNavigate()
    const [profileObject, setprofileObject] = useState({});
    const link = '/api/users/profile';
    const [listLength, setListLength] = useState(0);

    const handleListLength = (length) => {
      setListLength(length);
    };

    useEffect(() => {
      const fetchData = async () => {
        const access_token = localStorage.getItem('access_token');
        try {
          if (!access_token) {
            navigate('/auth');
          }
          console.log(access_token);
          const headers = {
            Authorization: `Bearer ${access_token}`,
          };
    
          const response = await fetch(link, { headers });
          if (!response.ok) {
            throw new Error('Ошибка при получении данных');
          }
          const data = await response.json();
          setprofileObject(data);
          console.log("Наши данные", data)    
        } catch (error) {
          console.log(error);}
      };
      fetchData();
    }, [navigate]);
    const Profile = Object(profileObject)
    const birthdate = new Date(profileObject.birthdate);
    const currentDate = new Date();
    console.log(currentDate)
    const ageInMilliseconds = currentDate - birthdate;
    console.log(ageInMilliseconds)
    const ageInYears = Math.floor(ageInMilliseconds / (1000 * 60 * 60 * 24 * 365));
    console.log(ageInYears)
    return (
    <section className="profile" id="profile">
        <div className="container">
          <div className="row">
              <h1 className="title">Профиль</h1>
          </div>
          {Object.entries(Profile).length > 0 && (
            <div className="row">
              <div className="col-xxl-3">
                <div className="image-block">
                  <img src={Profile.photo} alt="" className="avatar" />
                </div>
              </div>
              <div className="col-xxl-6">
                <p className="user_name">{Profile.name}</p> 
                <div className="row user-info age align-items-center">
                    <div className="col-xxl-2">
                        возраст
                    </div>
                    <div className="col-xxl-3 offset-xxl-1">
                        .......................
                    </div>
                    <div className="col-xxl-3">
                        {ageInYears} лет
                    </div>
                </div>
                <div className="row user-info tower align-items-center">
                    <div className="col-xxl-2">
                        Город
                    </div>
                    <div className="col-xxl-3 offset-xxl-1">
                        .......................
                    </div>
                    <div className="col-xxl-3">
                        Иваново
                    </div>
                </div>
                <div className="row user-info type align-items-center">
                    <div className="col-xxl-2">
                        Тип
                    </div>
                    <div className="col-xxl-3 offset-xxl-1">
                        .......................
                    </div>
                    <div className="col-xxl-3">
                        Поисковик
                    </div>
                </div>
                <div className="row user-info achievements align-items-center">
                    <div className="col-xxl-2">
                        Достижения
                    </div>
                    <div className="col-xxl-3 offset-xxl-1">
                        .......................
                    </div>
                    <div className="col-xxl-3">
                        2
                    </div>
                </div>
                <div className="row user-info caught__pets align-items-center">
                    <div className="col-xxl-2">
                        Успешно  пойманных  питомцев
                    </div>
                    <div className="col-xxl-3 offset-xxl-1">
                        .......................
                    </div>
                    <div className="col-xxl-3">
                        {listLength}
                    </div>
                </div>
              </div>
            </div>
            )}
            <div className={`row ${styles.aboutme}`}>
                  <h1 className={styles.title}>О себе</h1>
                  <p className={styles.content}>{Profile.about}</p>
            </div>
            <div className="row caughtpets text-center swiper-container">
              <h1 className="title">Последние найденные питомцы</h1>
              <SwiperSlidePet onListLengthChange={handleListLength}/>
            </div>
        </div>
    </section>
    );
}

export default Profile_block;
