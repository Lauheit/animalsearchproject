
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/swiper.min.css';
const Swiper = () => {
  return (
    <Swiper slidesPerView={3} spaceBetween={10} className="col-xxl-8">
      <SwiperSlide>
        <div className="col-xxl-2">
          <div className="avatar-block">
            <img src="./src/assets/images/dist/pet_1.png" alt="" className="avatar" />
          </div>
          <div className="name-pet">Рокки</div>
          <div className="data">Найдем в течении <span className="accent">5</span> часов</div>
          <div className="more">Подробнее</div>
        </div>
      </SwiperSlide>
      <SwiperSlide>
        <div className="col-xxl-2">
          <div className="avatar-block">
            <img src="./src/assets/images/dist/pet_1.png" alt="" className="avatar" />
          </div>
          <div className="name-pet">Тотошка</div>
        </div>
      </SwiperSlide>
      <SwiperSlide>
        <div className="col-xxl-2">
          <div className="avatar-block">
            <img src="./src/assets/images/dist/pet_1.png" alt="" className="avatar" />
          </div>
          <div className="name-pet">Трезор</div>
        </div>
      </SwiperSlide>
      <SwiperSlide>
        <div className="col-xxl-2">
          <div className="avatar-block">
            <img src="./src/assets/images/dist/pet_1.png" alt="" className="avatar" />
          </div>
          <div className="name-pet">Мухтар</div>
        </div>
      </SwiperSlide>
    </Swiper>
  );
}

export default Swiper;