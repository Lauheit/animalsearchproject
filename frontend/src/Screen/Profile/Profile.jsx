
import Profile_block from './components/Profile_block'
import Footer from '../Auth/components/Footer'
import Header from './components/Header'

const Profile = () => {
    return(
        <>
            <Header />
            <Profile_block />
            <Footer />
        </>
    )
}

export default Profile