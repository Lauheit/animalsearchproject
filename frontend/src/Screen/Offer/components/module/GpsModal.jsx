import { Modal, Form,  Col } from "react-bootstrap";
import styles from './ModalWindows.module.css'
import { YMaps, Map, Placemark, SearchControl } from '@pbe/react-yandex-maps';

export const Header_g = () => {
    return (
        <Modal.Title>Проверьте набранные данные</Modal.Title>
    )
}

export const Content_g = ({handleMapClick, coordinates}) => {
    return (
        <div>
           <YMaps query={{apikey: 'adec38e2-ee37-445f-807b-2ffd58853e7d'}}>
          <div>
            <Map
              defaultState={{ center: [55.751574, 37.573856], zoom: 10 }}
              width={400}
              height={400}
              onClick={handleMapClick}
            >
              <SearchControl />
              {coordinates.length > 0 && <Placemark geometry={coordinates} />}
            </Map>
          </div>
        </YMaps>
        </div>
    )
}

export const Footer_g = ({handleSaveClick}) => {
    return (
        <Col xxl="4">
          <div 
              className={`
                  w-100 btn 
                  ${styles.btncustom}
              `} 
              onClick={handleSaveClick}
          > Сохранить
          </div>
      </Col>
    )
}