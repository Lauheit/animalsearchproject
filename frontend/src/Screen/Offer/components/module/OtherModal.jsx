import React from "react";
import { Modal, Form,  Col } from "react-bootstrap";
import styles from './ModalWindows.module.css'

export const Header_o = () => {
    return <Modal.Title className={`${styles.title}`}>Кто пропал?</Modal.Title>;
  };

export const Content_o = ({ inputValue, handleChange }) => (
    <Form.Group controlId="exampleForm.ControlInput1">
      <Form.Control
        type="text"
        placeholder="Введите название питомца"
        value={inputValue}
        onChange={handleChange}
      />
    </Form.Group>
  );

export const Footer_o = ({handleSaveClick}) => {
    return (
      <Col xxl="4">
          <div 
              className={`
                  w-100 btn 
                  ${styles.btncustom}
              `} 
              onClick={handleSaveClick}
          > Сохранить
          </div>
      </Col>
    );
};
