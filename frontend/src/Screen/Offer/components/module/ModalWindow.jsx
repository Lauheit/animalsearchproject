import { Modal, Button, Row, Container, Form, FormLabel } from "react-bootstrap";
import { useState, useEffect } from "react";
import { Header_o, Content_o, Footer_o } from "./OtherModal";
import { Content_p, Footer_p, Header_p } from "./PhotoModal";
import Geocode from 'react-geocode';
import { Footer_g, Header_g, Content_g } from "./GpsModal";
import { Footer_c } from "./CheckOfferModal";
import sendDataToServer from "../../../../componetns/ui/JsonPost";
import { useNavigate } from "react-router-dom";
const ModalWindow = ({
  showModal,
  modalType,
  handleCloseModal,
  handleSave,
  handleSavePhoto,
  typeName,
  Desc,
  DescPluss,
  photoUrl,
  phoneNumber,
  namePet
}) => {
  const [inputValue, setInputValue] = useState("");
  const [selectedFile, setSelectedFile] = useState(null);
  const [address, setAddress] = useState('');
  const [coordinates, setCoordinates] = useState([]);
  const [error, setError] = useState('');
  const [response, setResponse] = useState('');
  const navigate = useNavigate()
  const addOffer = 'api/applications/add'
  const handleMapClick = async (e) => {
    try {
      if (e && e.latLng) {
        Geocode.setApiKey('6697171e-33f4-4b31-8788-a41ee8d03c07');
        
        const lat = e.latLng.lat();
        const lng = e.latLng.lng();
        
        const coords = [lat, lng];
        setCoordinates(coords);
        
        const response = await Geocode.fromLatLng(lat, lng);
        const firstGeoObject = response.results[0];
        setAddress(firstGeoObject.formatted_address);
      }
    } catch (error) {
      console.error('Ошибка при получении адреса:', error);
    }
  };

  useEffect(() => {
    const fetchAddress = async () => {
      if (coordinates.length > 0) {
        try {
          const response = await Geocode.fromLatLng(coordinates[0], coordinates[1]);
          const address = response.results[0].formatted_address;
          setAddress(address);
        } catch (error) {
          console.error('Ошибка при получении адреса:', error);
        }
      }
    };
    fetchAddress();
  }, [coordinates]);

  const handleChange = (e) => {
    setInputValue(e.target.value);
  };

  const handleFileChange = (e) => {
    setSelectedFile(e.target.files[0]);
  };

  const handleSaveClick = async () => {
    if (modalType === "other") {
      const newData = {
        inputValue: inputValue
      };
      handleSave(newData);
      setInputValue("");
    }
    if (modalType === "photo") {
      if (selectedFile) {
        const reader = new FileReader();
        reader.onloadend = () => {
          const photoUrl = reader.result;
          handleSavePhoto(photoUrl);
          setSelectedFile(null);
        };
        reader.readAsDataURL(selectedFile);
      }
    }
    if (modalType === 'gps') {
      setAddress(address);
      setCoordinates(coordinates);
      console.log(coordinates)
      console.log(address)
    }
    if (modalType === 'check') {
      const jsonData = {
        animal: typeName,
        photo: photoUrl,
        desc: Desc,
        animal_name: namePet,
        circ: DescPluss,
        tel: phoneNumber
      };
      const data = await sendDataToServer(jsonData, addOffer);
      if (data.error) {
        // Обработка ошибки
          setError(data.error);
          console.error(data.error);
        } else {
        // Обработка успешного ответа
          console.log('Заявка успешно создана')
          navigate("/");
        }
      }
  };

  const renderModalHeader = () => {
    if (modalType === 'other') {
      return <Header_o />;
    } else if (modalType === 'photo') {
      return <Header_p />;
    }
    if (modalType === 'gps') {
      return <Header_g />
    }
    if (modalType === 'check') {
      return null;
    }
  };

  const renderModalContent = () => {
    if (modalType === "other") {
      return <Content_o inputValue={inputValue} handleChange={handleChange} />;
    }
    if (modalType === "photo") {
      return <Content_p inputValue={inputValue} handleChange={handleChange} handleFileChange={handleFileChange} />;
    }
    if (modalType === 'gps') {
      return <Content_g coordinates={coordinates} handleMapClick={handleMapClick} />;
    }
    if (modalType === 'check') {
      return (
        <Form>
          <div>
            <FormLabel>Имя питомца: {namePet}</FormLabel>
          </div>
          <div>
            <FormLabel>Номер телефона: {phoneNumber}</FormLabel>
          </div>
          <div>
            <FormLabel>Тип питомца: {typeName}</FormLabel>
          </div>
          <div>
            <FormLabel>Описание питомца: {Desc}</FormLabel>
          </div>
          <div>
            <FormLabel>Как потерялся: {DescPluss}</FormLabel>
            </div>
          <div>
            <FormLabel>Местоположение: {address}</FormLabel>
          </div>
          <div>
            <FormLabel>Фото питомца: <img className={`w-100`} src={photoUrl} alt="Added photo" /></FormLabel>
          </div>
        </Form>
      );
    }
  };

  const renderModalFooter = () => {
    if (modalType === 'other') {
      return <Footer_o handleSaveClick={handleSaveClick} />;
    }
    if (modalType === 'photo') {
      return <Footer_p handleSaveClick={handleSaveClick} />;
    }
    if (modalType === 'gps') {
      return <Footer_g handleSaveClick={handleSaveClick} />;
    }
    if (modalType === 'check'){
      return <Footer_c handleSaveClick={handleSaveClick} />;
    }
  };

  return (
  <Container>
    <Row>
      <Modal show={showModal} onHide={handleCloseModal}>
        <Modal.Header closeButton>{renderModalHeader()}</Modal.Header>
        <Modal.Body>{renderModalContent()}</Modal.Body>
        <Modal.Footer>{renderModalFooter()}</Modal.Footer>
      </Modal>
    </Row>
  </Container>  
); };

export default ModalWindow;