import React from "react";
import { Modal, Form,  Col } from "react-bootstrap";
import styles from './ModalWindows.module.css'

export const Header_p = () => {
    return <Modal.Title className={`${styles.title}`}>Добавить фото</Modal.Title>;
}

export const Content_p = ({ inputValue, handleFileChange, handleChange }) => {
    return(
        <Form>
            <Form.Group controlId="formPhotoUrl">
                <Form.Control placeholder="Вставьте ссылку на фото" type="text" value={inputValue} onChange={handleChange} />
            </Form.Group>
            <Form.Group controlId="formPhotoFile">
                <Form.Control type="file" onChange={handleFileChange} accept="image/*" />
            </Form.Group>
        </Form>
    )
}

export const Footer_p = ({ handleSaveClick }) => {
    return (
      <>
        <Col xxl="4">
            <div 
                className={`
                    w-100 btn 
                    ${styles.btncustom}
                `} 
                onClick={handleSaveClick}
            > Сохранить
            </div>
        </Col>
      </>
    );
};