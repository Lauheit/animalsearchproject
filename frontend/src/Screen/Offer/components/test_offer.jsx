import { useState, useRef } from 'react';
import { Form, FormGroup, FormControl, FormLabel, Button, Row, Col, Container } from 'react-bootstrap';
import CustomTypeModal from './ModalWindowType';
import CustomPreviewModal from './ModalWindowPreview';
import InputMask from 'react-input-mask';
import styles from './module/offer.module.sass'
import classNames from 'classnames';

const PetForm = () => {
    const phoneNumberRef = useRef(null);
    const [loading, setLoading] = useState('')
    const [phone, setPhone] = useState('');
    const [petName, setPetName] = useState('');
    const [photo, setPhoto] = useState('');
    const [description, setDescription] = useState('');
    const [lostInfo, setLostInfo] = useState('');
    const [location, setLocation] = useState('');

    const [error, setError] = useState('')

    const [selectedType, setSelectedType] = useState('');
    const [showModalType, setShowModalType] = useState(false);
    const [showModalPreview, setShowModalPreview] = useState(false);
    const [customType, setCustomType] = useState('Введите тип питомца');

    const handlePetTypeChange = (value) => {
        setSelectedType(value);
    };

    const handleCustomTypeChange = (e) => {
        setCustomType(e.target.value);
    };

    const handleModalTypeOpen = () => {
        setShowModalType(true);
        setCustomType('')
    };

    const handleModalTypeClose = () => {
        setShowModalType(false);
    };

    const handleModalPreviewClose = () =>{
        setShowModalPreview(false);
    }

    const handleModalSave = () => {
        if (customType !== "") {
            setSelectedType(customType);
            console.log(customType)
        }
        setShowModalType(false);
    };




    const handleSubmit = (e) => {
        e.preventDefault();
        if (!selectedType || !photo || !phone || !description || !lostInfo || !location || !petName) {
            setError('Пожалуйста, заполните все поля');
            setLoading(false);
            return;
          }
        const jsonData = {
            selectedType,
            photo,
            phone,
            description,
            lostInfo,
            location,
            petName
        }
        // Действия при отправке формы
        // Например, отправка данных на сервер
        setShowModalPreview(true)
        console.log('обработанные данные', jsonData);
    };
    const variantClassDog = classNames({
        [styles.custom]: selectedType === 'dog',
        [styles.outcustom]: selectedType !== 'dog',
    });
    const variantClassCat = classNames({
        [styles.custom]: selectedType === 'cat',
        [styles.outcustom]: selectedType !== 'cat',
    });
    const variantClassBird = classNames({
        [styles.custom]: selectedType === 'bird',
        [styles.outcustom]: selectedType !== 'bird',
    });
    const variantClassOrder = classNames({
        [styles.custom]: selectedType === customType,
        [styles.outcustom]: selectedType !== customType,
    });
    return (
        <Container>
            <Row className={`${styles.marginb}`}>
                <Col xxl="3">
                    <h1 className={`${styles.title}`}>Заявка</h1>
                </Col>
            </Row>
                <Form onSubmit={handleSubmit}>
                    <FormGroup>
                        <Row className='mb-4'>
                            <Col xxl="3">
                            <FormLabel className={`${styles.title__form}`}>Номер телефона:</FormLabel>
                            </Col>
                            <Col xxl='3'>
                                <InputMask
                                    mask="+7 (999) 999-99-99"
                                    placeholder="Введите номер телефона"
                                    inputRef={phoneNumberRef}
                                    onChange={(e) => setPhone(e.target.value)}
                                    className="form-control"
                                />
                            </Col>
                        </Row>
                    </FormGroup>

                    <FormGroup>
                        <Row className='mb-4'>
                            <Col xxl="3">
                                <FormLabel className={`${styles.title__form}`}>Имя питомца:</FormLabel>
                            </Col>
                            <Col xxl='3'>
                                <FormControl
                                    type="text"
                                    value={petName}
                                    onChange={(e) => setPetName(e.target.value)}
                                />
                            </Col>
                        </Row>
                    </FormGroup>
                    <FormGroup>
                        <Row className='mb-4'>
                            <Col xxl="3">
                                <FormLabel className={`${styles.title__form}`}>Тип питомца:</FormLabel>
                            </Col>
                            <Col xxl="2">
                                <Button
                                    className={`w-100 ${variantClassDog}`}
                                    variant=''
                                    onClick={() => handlePetTypeChange("dog")}
                                    block={selectedType === "dog"}
                                >Собака
                                </Button>
                            </Col>
                            <Col xxl="2">
                                <Button
                                    className={`w-100 ${variantClassCat}`}
                                    variant=''
                                    onClick={() => handlePetTypeChange("cat")}
                                    block={selectedType === "cat"}
                                >Кошка
                                </Button>
                            </Col>
                            <Col xxl="2">
                                <Button
                                    className={`w-100 ${variantClassBird}`}
                                    variant=''
                                    onClick={() => handlePetTypeChange("bird")}
                                    block={selectedType === "bird"}
                                >Птица
                                </Button>
                            </Col>
                            <Col xxl="2">
                                <Button
                                    className={`w-100 ${variantClassOrder}`}
                                    variant=''
                                    onClick={() => handleModalTypeOpen()}
                                    block={selectedType === customType}>
                                    {selectedType !== customType ? "Другое" : customType}
                                </Button>
                            </Col>
                                {/*  <Button
                                variant={selectedType === "other" ? "primary" : "outline-primary"}
                                onClick={() => {
                                setSelectedType(selectedType === "other" ? "" : "other");
                                setShowModal(true);
                                }}
                                block={selectedType === "other"}
                                >
                                {selectedType === "other" ? customType : "Другое"}
                                </Button> */}
                        </Row>
                    </FormGroup>
                    <FormGroup>
                        <Row className='mb-4'>
                            <Col xxl="3">
                                <FormLabel className={`${styles.title__form}`}>Фото:</FormLabel>
                            </Col>
                            <Col xxl='3'>
                                <FormControl
                                    type="file"
                                    accept="image/*"
                                    onChange={(e) => {
                                    const file = e.target.files[0];
                                    const reader = new FileReader();
                                    reader.onload = (event) => {
                                    setPhoto(event.target.result);
                                    };
                                    reader.readAsDataURL(file);
                                    }}
                                />
                            </Col>
                        </Row>
                    </FormGroup>
                    {photo && <Row className='mb-4'>
                        <Col xxl="3">
                        <img src={photo} className={`w-100`} alt="Выбранное фото" />
                        </Col>
                    </Row>
                    }
                    <FormGroup>
                        <Row className='mb-4'>
                            <Col xxl="3">
                                <FormLabel className={`${styles.title__form}`}>Описание:</FormLabel>
                            </Col>
                            <Col xxl='7'>
                                <FormControl
                                    resize="none"
                                    as="textarea"
                                    className={`${styles['textarea-height']} ${styles.textarea}`}
                                    value={description}
                                    onChange={(e) => setDescription(e.target.value)}
                                />
                            </Col>
                        </Row>
                    </FormGroup>

                    <FormGroup>
                        <Row className='mb-4'>
                            <Col xxl="3">
                                <FormLabel className={`${styles.title__form}`}>Как пропал:</FormLabel>
                            </Col>
                            <Col xxl='7'>
                                <FormControl
                                    as="textarea"
                                    className={`${styles['textarea-height']} ${styles.textarea}`}
                                    value={lostInfo}
                                    onChange={(e) => setLostInfo(e.target.value)}
                                />
                            </Col>
                        </Row>
                    </FormGroup>

                    <FormGroup>
                        <Row className='mb-4'>
                            <Col xxl="3">
                                <FormLabel className={`${styles.title__form}`}>Местоположение:</FormLabel>
                            </Col>
                            <Col xxl='7'>
                                <FormControl
                                    type="text"
                                    value={location}
                                    onChange={(e) => setLocation(e.target.value)}
                                />
                            </Col>
                        </Row>
                    </FormGroup>
                    <FormGroup className={`${styles.margin}`}>
                        <Row className='mt-5 justify-content-center'>
                            <Col xxl="3">
                                <Button 
                                    type="submit" 
                                    variant='' 
                                    className={`w-100 ${styles.btn}`}
                                > Отправить
                                </Button>
                            </Col>
                        </Row>
                    </FormGroup>
                    {error && <p>{error}</p>}
                    <CustomTypeModal
                        showModal={showModalType}
                        handleModalClose={handleModalTypeClose}
                        handleModalSave={handleModalSave}
                        customType={customType}
                        handleCustomTypeChange={handleCustomTypeChange}
                    />
                    <CustomPreviewModal
                        showModal={showModalPreview}
                        handleModalClose={handleModalPreviewClose} 
                        selectedType={selectedType}
                        photo={photo}
                        phone={phone}
                        description={description}
                        lostInfo={lostInfo}
                        location={location}
                        petName={petName}
                    />
                </Form>
        </Container>
    );
};

export default PetForm;
