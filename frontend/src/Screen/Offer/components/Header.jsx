import { Link } from "react-router-dom"

const Header = () => {
    return(
        <header className="header" id="header">
            <div className="container">
                <nav className="navbar navbar-expand-lg font-size">
                    <div className="col-xxl-3 d-flex">
                        <Link to={"/"} className="navbar-brand"><div className="logotip"></div></Link>
                    </div>
                    <div className="col-xxl-4">
                        <div className="collapse navbar-collapse text-uppercase" id="navbarSupportedContent">
                            <ul className="navbar-nav">
                                <li className="nav-item">
                                    <Link to={"/#easysteps"} className="nav-link">Помощь</Link>
                                </li>
                                <li className="nav-item">
                                    <Link to={"/#aboutme"} className="nav-link">О нас</Link>
                                </li>
                                <li className="nav-item">
                                    <Link to={"/#footer"} className="nav-link">Отзывы</Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="col-xxl-1 offset-4 text-uppercase text-end">
                        <Link to={"/profile"} className="nav-link">Профиль</Link>
                    </div>
                </nav>
            </div>
        </header>
    )
}

export default Header