import React, { useState } from 'react';
import { Modal, Button, FormGroup, FormLabel, FormText } from 'react-bootstrap';
import sendDataToServer from '../../../componetns/ui/JsonPost';
import { useNavigate } from 'react-router';
const CustomPreviewModal = ({ 
    showModal, 
    handleModalClose, 
    selectedType,
    photo,
    phone,
    description,
    lostInfo,
    location,
    petName,
   }) => {
    const navigate = useNavigate()
    const linkOfferAdd = 'api/applications/add'
    const [error, setError] = useState("")
    const handleModalSubmit = async () =>{
        const formattedPhoneNumber = phone.replace(/\D/g, '');
        const jsonData = {
            animal: selectedType,
            photo: photo,
            description: description,
            animal_name: petName,
            circ: lostInfo,
            tel: formattedPhoneNumber,
            status : "Новое"
        }
        try {
            const data = await sendDataToServer(jsonData, linkOfferAdd)
            if (data.hasOwnProperty('error')) {
              setError(data.error);
              console.log(data.error);
            } else {
              const successResponseMsg = data.msg;
              console.log(successResponseMsg)
              navigate('/')
          }
          } catch (error) {
              setError("Произошла ошибка при отправке формы.");
              console.error(error);
          }
    }
  return (
    <Modal show={showModal} onHide={handleModalClose}>
      <Modal.Header closeButton>
        <Modal.Title>Проверьте введеные данные</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <FormGroup>
          <FormLabel>Ваш номер телефона</FormLabel>
          <FormText> {phone}</FormText>
        </FormGroup>
        <FormGroup>
          <FormLabel>Имя питомца</FormLabel>
          <FormText> {petName}</FormText>
        </FormGroup>
        <FormGroup>
          <FormLabel>Тип питомца</FormLabel>
          <FormText> {selectedType}</FormText>
        </FormGroup>
        <FormGroup>
          <FormLabel>Описание питомца</FormLabel>
          <FormText> {description}</FormText>
        </FormGroup>
        <FormGroup>
          <FormLabel>Как пропал ваш питомец</FormLabel>
          <FormText> {lostInfo}</FormText>
        </FormGroup>
        <FormGroup>
          <FormLabel>Местоположение</FormLabel>
          <FormText> {location}</FormText>
        </FormGroup>
        <FormGroup>
          <FormLabel>Фото питомца</FormLabel>
          {photo && <img src={photo} className='w-100' alt="Выбранное фото" />}
        </FormGroup>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="primary" onClick={handleModalSubmit}>Начать поиски</Button>
      </Modal.Footer>
    </Modal>
  );
};

export default CustomPreviewModal;
