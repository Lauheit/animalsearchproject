import React from 'react';
import { Modal, Button, FormControl } from 'react-bootstrap';

const CustomTypeModal = ({ showModal, handleModalClose, handleModalSave, customType, handleCustomTypeChange }) => {
  return (
    <Modal show={showModal} onHide={handleModalClose}>
      <Modal.Header closeButton>
        <Modal.Title>Введите тип питомца</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <FormControl
          type="text"
          value={customType}
          onChange={handleCustomTypeChange}
        />
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleModalClose}>Отмена</Button>
        <Button variant="primary" onClick={handleModalSave}>Сохранить</Button>
      </Modal.Footer>
    </Modal>
  );
};

export default CustomTypeModal;
