import { useState, useRef } from "react";
import ModalWindow from "./module/ModalWindow";
import styles from './module/Offer_block.module.sass'
import { Row,Col, Form } from "react-bootstrap";
import InputMask from "react-input-mask";
const Offer_block = () => {
    const phoneNumberRef = useRef(null);
    const [phoneNumber, setPhoneNumber] = useState('')
    const [showModal, setShowModal] = useState(false);
    const [otherValue, setOtherValue] = useState("");
    const [error, setError] = useState("")
    const [modalType, setModalType] = useState("");
    const [photoUrl, setPhotoUrl] = useState("");
    const [petValue, setPetValue] = useState("");
    const [namePet, setNamePet] = useState("")
    const [activeButton, setActiveButton] = useState(null);
    const [selectedLocation, setSelectedLocation] = useState(null);
    const [searchAddress, setSearchAddress] = useState('');
    const [Desc, setDesc] = useState('');
    const [DescPluss, setDescPluss] = useState('');
    const [typeName, setTypeName] = useState('')

    const handleClickCheck = () =>{
        // if (!Desc || !phoneNumber || !DescPluss || !typeName || !namePet || photoUrl) {
        //     setError('Пожалуйста, заполните все поля');
        //     return;
        // }
        const jsonData = {
            animal: typeName,
            photo: photoUrl,
            desc: Desc,
            animal_name: namePet,
            circ: DescPluss,
            tel: phoneNumber
          };
        console.log(jsonData)
        handleShowModal("check")
    }

    const handleButtonClick = (buttonIndex) => {
        if (buttonIndex === activeButton) { setActiveButton(null);
        } else { 
            if (buttonIndex === 1) {
                handleSaveOtherValue("Собака")
            }
            if (buttonIndex === 2) {
                handleSaveOtherValue("Кошка")
            }
            if (buttonIndex === 3){
                handleSaveOtherValue("Птица")
            }
            setActiveButton(buttonIndex);
        }
    };
    const handleButtonClickModal = () => {
        // Функция, которая вызывает две функции
        handleShowModal("other");
        handleButtonClick(4);
    };
    const handleCloseModal = () => {
        setShowModal(false);
    };

    const handleShowModal = (type) => {
        setModalType(type);
        setShowModal(true);
    };

    const handleChangeDesc = (event) => {
        setDesc(event.target.value);
    };

    const handleChangeDescPluss = (event) => {
        setDescPluss(event.target.value);
    };

    const handleSaveOtherValue = (value) => {
        if (value === 'Собака'){
            setPetValue(value);
            const TypeName = petValue
            setTypeName(TypeName)
        }else if(value === 'Кошка'){
            setPetValue(value);
            const TypeName = petValue
            setTypeName(TypeName)
        }else if(value === 'Птица'){
            setPetValue(value);
            const TypeName = petValue
            setTypeName(TypeName)
        }else{
            const TypeName = otherValue
            setTypeName(TypeName)
        }
        handleCloseModal();
    };

    const handleSavePhoto = (photoUrl) => {
        console.log("Saving photo:", photoUrl);
        setPhotoUrl(photoUrl);
        handleCloseModal();
    };

    const handleLocationSave = (location) => {
        setSelectedLocation(location);
    };
    
    const handleAddressSave = (searchAddress) => {
        setSearchAddress(searchAddress);
    };

    return(
        <section className="offer">
            <div className="container">
                <div className="row">
                    <h1 className="title">Заявка</h1>
                </div>
                <div className="offer__main">
                    <div className="phoneNumber">
                        <Row>
                            <Col xxl="3">
                                <h1 className="title">Имя питомца</h1>
                            </Col>
                            <Col xxl="3">
                            <Form.Control type="text" placeholder="Введите имя питомца" value={namePet} onChange={(e) => setNamePet(e.target.value)} />
                            </Col>
                        </Row>
                    </div>
                    <div className="phoneNumber">
                        <Row>
                            <Col xxl="3">
                                <h1 className="title">Номер телефона</h1>
                            </Col>
                            <Col xxl="3">
                                <InputMask
                                    mask="+7 (999) 999-99-99"
                                    placeholder="Введите номер телефона"
                                    inputRef={phoneNumberRef}
                                    onChange={(e) => setPhoneNumber(e.target.value)}
                                    className="form-control"
                                />
                            </Col>
                        </Row>
                    </div>
                    <div className="main-who offer__margin">
                        <div className="row">
                            <div className="col-xxl-3">
                                <h1 className="title">Кто пропал</h1>
                            </div>
                            <div className="col-xxl-2">
                                <div
                                className={`btn ${styles.btncustom} ${activeButton === 1 ? styles.active : ""}`}
                                onClick={() => handleButtonClick(1)}
                                >
                                Собака
                                </div>
                            </div>
                            <div className="col-xxl-2">
                                <div
                                className={`btn ${styles.btncustom} ${activeButton === 2 ? styles.active : ""}`}
                                onClick={() => handleButtonClick(2)}
                                >
                                Кошка
                                </div>
                            </div>
                            <div className="col-xxl-2">
                                <div
                                className={`btn ${styles.btncustom} ${activeButton === 3 ? styles.active : ""}`}
                                onClick={() => handleButtonClick(3)}
                                >
                                Птица
                                </div>
                            </div>
                            <div className="col-xxl-2">
                            {otherValue ? (
                                <div 
                                    className={`btn ${styles.btncustom} ${activeButton === 4 ? styles.active : ""}`}
                                    onClick={() => handleButtonClickModal()}
                                >{otherValue}</div>
                            ) : (
                                <div 
                                    className={`btn ${styles.btncustom}`}
                                    onClick={() => handleButtonClickModal()}
                                > Другое
                                </div>
                            )}
                            </div>
                        </div>
                    </div>
                    <div className="add-photo offer__margin">
                        <div className="row">
                            <div className="col-xxl-3">
                                <h1 className="title">Фото</h1>
                            </div>
                            {photoUrl && (
                            <div className="col-xxl-2">
                                <div><img className={`w-100 ${styles.image}`} src={photoUrl} alt="Added photo" /></div>
                            </div>
                            )}
                            <div className="col-xxl-2">
                                <button className="btn btn-custom-accent" onClick={() => handleShowModal("photo")}>Добавить</button>
                            </div>
                        </div>
                    </div>
                    <div className="description offer__margin">
                        <div className="row">
                            <div className="col-xxl-3">
                                <h1 className="title">Описание</h1></div>
                            <div className="col-xxl-8">
                                <textarea 
                                    className="form-control form-control-custom" 
                                    placeholder="Введите описание вашего питомца..." 
                                    value={Desc}
                                    onChange={handleChangeDesc}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="How-who offer__margin">
                        <div className="row">
                            <div className="col-xxl-3">
                                <h1 className="title">Как потерялся</h1></div>
                            <div className="col-xxl-8">
                                <textarea 
                                    className="form-control form-control-custom" 
                                    placeholder="Укажите обстоятельства пропажи" 
                                    value={DescPluss}
                                    onChange={handleChangeDescPluss}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="address offer__margin">
                        <div className="row">
                            <div className="col-xxl-3">
                                <h1 className="title">Местоположение</h1></div>
                            <div className="col-xxl-2">
                                <button 
                                className={`btn ${styles.btncustom}`} 
                                onClick={() => handleShowModal("gps")}
                                >Указать</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="start row justify-content-center">
                    <div className="col-xxl-3">
                    <button 
                        className={`w-100 btn ${styles.btncustom}`} 
                        onClick={() => handleClickCheck()}
                    >Отправить
                    </button>
                    </div>
                </div>
                {error && <p>{error}</p>}
            </div>
            <ModalWindow
                showModal={showModal}
                modalType={modalType}
                handleCloseModal={handleCloseModal}
                handleSave={handleSaveOtherValue}
                handleSavePhoto={handleSavePhoto}
                handleLocationSave={handleLocationSave}
                handleAddressSave={handleAddressSave}
                typeName={typeName}
                Desc={Desc}
                DescPluss={DescPluss}
                photoUrl={photoUrl}
                phoneNumber={phoneNumber}
                namePet={namePet}
            />
        </section>
    )
}

export default Offer_block