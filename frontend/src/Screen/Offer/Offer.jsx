import Header from "./components/Header"
import Offer_block from "./components/Offer_block"
import Footer from '../Auth/components/Footer'
import PetForm from "./components/test_offer"

const Offer = () => {
    return(
        <>
            <Header />
            <PetForm />
            <Footer />
        </>
    )
}

export default Offer