import { useState, useRef } from "react";
import { InputGroup, Row, Form, Button, Image, Container, Modal, Col } from "react-bootstrap"
import InputMask from 'react-input-mask';
import styles from './module/registration.module.sass'
import { Verify } from "../../../componetns/smsverefy";
import sendDataToServer from "../../../componetns/ui/JsonPost";
import { useNavigate } from "react-router";


const Register = () => {
  const phoneNumberRef = useRef(null);
  const [smsCode, setSmsCode] = useState('');
  const [responseSms, setResponseSms] = useState({});
  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);
  const [phoneNumber, setPhoneNumber] = useState('')
  const [avatar, setAvatar] = useState(null);
  const [avatarPreview, setAvatarPreview] = useState(null);
  const [fullName, setFullName] = useState('');
  const [showModal, setShowModal] = useState(false);
  const [birthdate, setBirthdate] = useState('');
  const [about, setAbout] = useState('');
  const [finalNumber, setFinalNumber] = useState('');
  const [birthdateError, setBirthdateError] = useState('');
  const [avatarData, setAvatarData] = useState(null);
  const [errorCheckPhone, setErrorCheckPhone] = useState('')
  const linkreg = '/api/users/registration'
  const linkVerify = '/api/users/sms_verify';
  const linkCheckphone = '/api/users/check_phone'
  const navigate = useNavigate()
  const handleModalShow = () => {
    setShowModal(true);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    if (!fullName || !phoneNumber || !avatar || !birthdate || !about) {
      setError('Пожалуйста, заполните все поля');
      setLoading(false);
      return;
    }
    console.log(birthdate)
    // Проверка на верную дату рождения
    const currentDate = new Date();
    const selectedDate = new Date(birthdate);

    if (selectedDate >= currentDate) {
      setBirthdateError('Дата рождения должна быть в прошлом');
      return;
    }
    const formattedPhoneNumber = phoneNumber.replace(/\D/g, '');
    const jsonData = {
        phoneNumber: formattedPhoneNumber,
        option: "registration"
    }
    setFinalNumber(formattedPhoneNumber);
    const response = await fetch(linkCheckphone, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(jsonData)
      });
    const data = await response.json();

    if (response.ok) {
      VerifySms()
    }else{
      const error = data.error;
      setErrorCheckPhone(error)
    }
  };

const VerifySms = async () => {
  const jsonData = {
    phoneNumber: finalNumber
  } 
  try {
    const response = await Verify(jsonData, linkVerify);
  
    if (response.hasOwnProperty('error')) {
      setError(response.error);
      console.log(response.error);
    } else {
      const successResponse = response.sms_code;
      handleModalShow()
      setResponseSms(successResponse)
    }
  
  } catch (error) {
    setError("Произошла ошибка при отправке формы.");
    console.error(error);
  } finally {
    setLoading(false);
  }
}

const handleModalClose = () => {
    setShowModal(false);
    setSmsCode('');
};

const handleModalSubmit = async () => {
    setLoading(true);
    const jsonData = {
        sms_user: smsCode
    }
    try {
    const response = await Verify(jsonData, linkVerify);

    if (response.hasOwnProperty('error')) {
        setError(response.error);
        console.log(response.error);
      } else {
        const successResponse = response.secret;
        setShowModal(false);
        setSmsCode('');
        RegistrationUser(response.secret);
    }
    } catch (error) {
        setError("Произошла ошибка при отправке формы.");
        console.error(error);
    } finally {
        setLoading(false);
    }
  };
  const RegistrationUser = async (successResponse) => {
    const jsonData = {
      secret: successResponse,
      phoneNumber: finalNumber,
      name : fullName,
      photo : avatarData,
      about : about,
      birthdate : birthdate
    };
    console.log('Данные формы:', jsonData);
    try {
      const data = await sendDataToServer(jsonData, linkreg)
      if (data.hasOwnProperty('error')) {
        setError(data.error);
        console.log(data.error);
      } else {
        const successResponseId = data.id_user;
        const susccessResponseToken = data.access_token
        localStorage.setItem('access_token', susccessResponseToken);
        navigate('/profile')
    }
    } catch (error) {
        setError("Произошла ошибка при отправке формы.");
        console.error(error);
    } finally {
        setLoading(false);
    }
  };

  const handleAvatarChange = (e) => {
    const selectedAvatar = e.target.files[0];
    setAvatar(selectedAvatar);
    setAvatarPreview(URL.createObjectURL(selectedAvatar));
  
    const reader = new FileReader();
    reader.onloadend = () => {
      setAvatarData(reader.result);
    };
    reader.readAsDataURL(selectedAvatar);
  };

  const handleFullNameChange = (e) => {
    setFullName(e.target.value);
  };

  const handleBirthdateChange = (e) => {
    setBirthdate(e.target.value);
    setBirthdateError('');
  };

  const handlePhoneNumberChange = (e) => {
    // Обработка изменения значения номера телефона
    // В этом примере просто выводим значение в консоль
    setPhoneNumber(e.target.value);
  };

  const handleAboutChange = (e) => {
    setAbout(e.target.value);
  };

  return (
    <section className="">
        <Container>
          <div className={`${styles.margin}`}>
            <Row className="justify-content-center ">
              <Col xxl="5">
                  <h1 className={`text-center ${styles.title}`}>Регистрация</h1>
              </Col>
            </Row>
            <Row className="justify-content-center">
              <Col xxl="4">
                  <div className={`text-center ${styles.content}`}>Заполните все поля для регистрации</div>
              </Col>
            </Row>
          </div>
            <Form onSubmit={handleSubmit} className="">
              <Form.Group controlId="fullName" className="d-flex mb-4">
                <Col xxl="3">
                  <Form.Label className={`${styles.title__form}`}>ФИО</Form.Label>
                </Col>
                <Col xxl="3">
                  <Form.Control className={`${styles.field}`} variant="" type="text" placeholder="Введите ФИО" value={fullName} onChange={handleFullNameChange} />
                </Col>
              </Form.Group>

              <Form.Group controlId="phoneNumber" className="d-flex mb-4">
                <Col xxl="3">
                  <Form.Label className={`${styles.title__form}`}>Номер телефона</Form.Label>
                </Col>
                <Col xxl="3">
                  <InputMask
                  
                  mask="+7 (999) 999-99-99"
                  placeholder="Введите номер телефона"
                  inputRef={phoneNumberRef}
                  onChange={handlePhoneNumberChange}
                  className={`form-control ${styles.field}`}
                  />
                </Col>
              </Form.Group>
              {errorCheckPhone && <p>{errorCheckPhone}</p>}

              <Form.Group controlId="avatar" className="d-flex mb-4">
                <Col xxl="3">
                  <Form.Label className={`${styles.title__form}`}>Ваше фото</Form.Label>
                </Col>
                <Col xxl="3">
                  <Form.Control 
                  className={`${styles.field}`}
                  variant="" 
                  type="file" 
                  accept="image/*" 
                  onChange={handleAvatarChange} />
                  {avatarPreview && <Image src={avatarPreview} alt="Аватар" className={`${styles.image}`} thumbnail />}
                </Col>
              </Form.Group>
              <Form.Group controlId="birthdate" className="d-flex mb-4">
                <Col xxl="3">
                  <Form.Label 
                  className={`${styles.title__form}`}>Дата рождения</Form.Label>
                </Col>
                <Col xxl="3">
                  <Form.Control 
                    className={`${styles.field}`}
                    type="date"
                    value={birthdate} 
                    onChange={handleBirthdateChange} />
                  {birthdateError && <Form.Text className={`${styles.image} text-danger`}>{birthdateError}</Form.Text>}
                </Col>
              </Form.Group>
              <Form.Group controlId="about" className="d-flex mb-4">
                <Col xxl="3">
                  <Form.Label className={`${styles.title__form}`}>О себе</Form.Label>
                </Col>
                <Col xxl="7">
                  <Form.Control className={`${styles.field}`} as="textarea" rows={3} placeholder="Расскажите о себе" value={about} onChange={handleAboutChange} />
                </Col>
              </Form.Group>
              <Row className="justify-content-center">
                <Col xxl="3">
                <Button className={`w-100 ${styles.btn} ${styles.marginbtn}`} variant="" type="submit">
                  Отправить 
                </Button>
                </Col>
              </Row>
            </Form>
            <Modal show={showModal} onHide={handleModalClose}>
                <Modal.Header closeButton>
                <Modal.Title>Введите код SMS {responseSms}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                <Form.Control
                    type="text"
                    placeholder="Введите код SMS"
                    value={smsCode}
                    onChange={(e) => setSmsCode(e.target.value)}
                />
                </Modal.Body>
                <Modal.Footer>
                <Button variant="secondary" onClick={handleModalClose}>
                Закрыть
            </Button>
            <Button variant="primary" onClick={handleModalSubmit}>
                Отправить
            </Button>
            </Modal.Footer>
        </Modal>
        </Container>
    </section>
    ); 
};

export default Register;
