import Header_Nav from "../../componetns/ui/Header"
import Footer from "../Auth/components/Footer"
import Register from "./components/Register"

const Registration = () => {
    return (
        <>
        <header className="header" id="header">
            <div className="container">
                <Header_Nav />
            </div>
        </header>
        <Register />
        <Footer />
        </>
    )
}

export default Registration
