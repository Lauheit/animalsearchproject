import Footer from "./components/Footer"
import Auth_block from "./components/Auth"
import Header_Nav from "../../componetns/ui/Header"

const Auth = () => {
    return (
        <>
        <header className="header" id="header">
            <div className="container">
                <Header_Nav />
            </div>
        </header>
        <Auth_block />
        <Footer />
        </>
    )
}

export default Auth