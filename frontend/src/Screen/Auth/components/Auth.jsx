import PhoneNumberForm from "./modules/test";

const Auth_block = () => {
  return (
    <section className="auth">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-xxl-5">
            <h1 className="title text-center">Авторизация</h1>
          </div>
        </div>
        <div className="row justify-content-center">
          <div className="col-xxl-4">
            <div className="content text-center">Введите номер телефона, чтобы  получить доступ ко всем возможностям</div>
          </div>
        </div>
          <PhoneNumberForm />
      </div>
    </section>
  );
};

export default Auth_block;
