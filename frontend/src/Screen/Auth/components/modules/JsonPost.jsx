const sendDataToServer = (data) => {
  fetch('localhost/api/users/sms_verify', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  })
  .then(response => response.json())
  .then(data => {
    // Обработка успешного ответа от сервера
    console.log(data);
    return data
  })
  .catch(error => {
    // Обработка ошибки при отправке данных на сервер
    console.error(error);
  });
};
export default sendDataToServer;