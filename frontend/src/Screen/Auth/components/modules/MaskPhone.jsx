import React from 'react';
import NumberFormat from 'react-number-format/dist/react-number-format';

const formatPhoneNumber = (phoneNumber) => {
  const phoneNumberWithoutSpaces = phoneNumber.replace(/\s/g, ''); // Удаляем все пробелы из номера телефона

  if (phoneNumberWithoutSpaces.length <= 3) {
    return phoneNumberWithoutSpaces; // Если номер телефона содержит меньше или равно 3 символам, то возвращаем его без изменений
  }

  let formattedPhoneNumber = '+7 ('; // Начинаем форматирование номера телефона с "+7 ("

  // Добавляем первые 3 символа номера телефона
  formattedPhoneNumber += phoneNumberWithoutSpaces.substr(0, 3);

  if (phoneNumberWithoutSpaces.length > 3) {
    // Добавляем пробел после первых 3 символов номера телефона
    formattedPhoneNumber += ') ';

    // Добавляем следующие 3 символа номера телефона
    formattedPhoneNumber += phoneNumberWithoutSpaces.substr(3, 3);
  }

  if (phoneNumberWithoutSpaces.length > 6) {
    // Добавляем пробел после следующих 3 символов номера телефона
    formattedPhoneNumber += ' ';

    // Добавляем остальные символы номера телефона
    formattedPhoneNumber += phoneNumberWithoutSpaces.substr(6);
  }

  // Возвращаем отформатированный номер телефона
  return <NumberFormat value={formattedPhoneNumber} displayType="text" format="+7 (###) ###-####" />;
};
export default formatPhoneNumber;