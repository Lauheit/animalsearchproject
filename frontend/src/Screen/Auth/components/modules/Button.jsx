import React from 'react';
const Button = ({ onSaveJson }) => {
  const handleClick = () => {
    onSaveJson();
  };
  return (
    <button onClick={handleClick} type="submit" className="btn btn-custom">Продолжить</button>
  );
};

export default Button;