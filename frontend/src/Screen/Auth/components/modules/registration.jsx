import { useState, useRef } from "react";
import { InputGroup, Row, Form, Button, Image } from "react-bootstrap"
import InputMask from 'react-input-mask';
import styles from './registration.module.sass'
import sendDataToServer from '../../../../componetns/ui/JsonPost'


const Registration = () => {
  const phoneNumberRef = useRef(null);
  const [phoneNumber, setPhoneNumber] = useState('')
  const [avatar, setAvatar] = useState(null);
  const [avatarPreview, setAvatarPreview] = useState(null);
  const [fullName, setFullName] = useState('');
  const [birthdate, setBirthdate] = useState('');
  const [about, setAbout] = useState('');
  const [birthdateError, setBirthdateError] = useState('');
  const [avatarData, setAvatarData] = useState(null);
  const reg = 'localhost/api/users/auth'
  const handleSubmit = async (e) => {
    e.preventDefault();

    // Проверка на верную дату рождения
    const currentDate = new Date();
    const selectedDate = new Date(birthdate);

    if (selectedDate >= currentDate) {
      setBirthdateError('Дата рождения должна быть в прошлом');
      return;
    }
    const jsonData = {
      phoneNumber,
      avatar: avatarData,
      fullName,
      birthdate,
      about
    };
    /* try {
      const data = await sendDataToServer(jsonData, reg);

      if (data.error) {
          setError(data.error);
          console.error(data.error);
      } else {
          setShowModal(false);
          setSmsCode('');
          ContinueUser(data.secret);
      }
      } catch (error) {
      console.error(error);
      setError("Произошла ошибка при отправке формы.");
      } */
    console.log('Данные формы:', jsonData);
  };

  const handleAvatarChange = (e) => {
    const selectedAvatar = e.target.files[0];
    setAvatar(selectedAvatar);
    setAvatarPreview(URL.createObjectURL(selectedAvatar));
  
    const reader = new FileReader();
    reader.onloadend = () => {
      setAvatarData(reader.result.replace(/^data:(.*;base64,)?/, ''));
    };
    reader.readAsDataURL(selectedAvatar);
  };

  const handleFullNameChange = (e) => {
    setFullName(e.target.value);
  };

  const handleBirthdateChange = (e) => {
    setBirthdate(e.target.value);
    setBirthdateError('');
  };

  const handlePhoneNumberChange = (e) => {
    // Обработка изменения значения номера телефона
    // В этом примере просто выводим значение в консоль
    setPhoneNumber(e.target.value);
  };

  const handleAboutChange = (e) => {
    setAbout(e.target.value);
  };

  return (
    <Form onSubmit={handleSubmit}>
      <Form.Group controlId="fullName">
        <Form.Label>ФИО</Form.Label>
        <Form.Control type="text" placeholder="Введите ФИО" value={fullName} onChange={handleFullNameChange} />
      </Form.Group>

      <Form.Group controlId="phoneNumber">
        <Form.Label>Номер телефона</Form.Label>
        <InputMask
          mask="+7 (999) 999-99-99"
          placeholder="Введите номер телефона"
          inputRef={phoneNumberRef}
          onChange={handlePhoneNumberChange}
          className="form-control"
        />
      </Form.Group>

      <Form.Group controlId="avatar">
        <Form.Label>Добавление аватара</Form.Label>
        <Form.Control type="file" accept="image/*" onChange={handleAvatarChange} />
        {avatarPreview && <Image src={avatarPreview} alt="Аватар" className={`${styles.image}`} thumbnail />}
      </Form.Group>


      <Form.Group controlId="birthdate">
        <Form.Label>Дата рождения</Form.Label>
        <Form.Control type="date" value={birthdate} onChange={handleBirthdateChange} />
        {birthdateError && <Form.Text className={`${styles.image} text-danger`}>{birthdateError}</Form.Text>}
      </Form.Group>

      <Form.Group controlId="about">
        <Form.Label>О себе</Form.Label>
        <Form.Control as="textarea" rows={3} placeholder="Расскажите о себе" value={about} onChange={handleAboutChange} />
      </Form.Group>

      <Button variant="primary" type="submit">
        Отправить 
    </Button>
    </Form>
    ); 
};

export default Registration;
