import React, { useState } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';

const ModalSms = ({response}) => {
  const [showModal, setShowModal] = useState(false);

  const handleClose = () => {
    setShowModal(false);
  };

  const handleShow = () => {
    setShowModal(true);
  };

  return (
    <>
      <Button variant="primary" onClick={handleShow}>
        Открыть модальное окно
      </Button>

      <Modal show={showModal} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Введите код который вы получили</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form onSubmit={handleSubmit}>
            <Form.Group controlId="formInput">
                <Form.Label>Код {response}</Form.Label>
                <InputGroup>
                <Form.Control
                    type="text"
                    value={inputValue}
                    onChange={handleChange}
                    placeholder="Введите значение"
                />
                <InputGroup.Append>
                    <Button type="submit">Отправить</Button>
                </InputGroup.Append>
            </InputGroup>
        </Form.Group>
        </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Закрыть
          </Button>
          <Button variant="primary" onClick={handleClose}>
            Сохранить
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default ModalSms;