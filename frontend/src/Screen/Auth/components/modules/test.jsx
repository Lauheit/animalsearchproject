import React, { useState, useRef } from 'react';
import { Col, Form, Row, Modal, Button, Container } from 'react-bootstrap';
import Select from "react-flags-select";
import { Link, useNavigate } from 'react-router-dom';
import { Verify } from '../../../../componetns/smsverefy';
import InputMask from 'react-input-mask';
import styles from './style.module.sass'
//import 'react-flags-select/build/components/ReactFlagsSelect/ReactFlagsSelect.module.scss';

const PhoneNumberForm = () => {
    const phoneNumberRef = useRef(null);
    const [errorCheckPhone, setErrorCheckPhone] = useState('')
    const [phoneNumber, setPhoneNumber] = useState('');
    const [error, setError] = useState('');
    const [showModal, setShowModal] = useState(false);
    const [smsCode, setSmsCode] = useState('');
    const [loading, setLoading] = useState(false);
    const [finalNumber, setFinalNumber] = useState('');
    const [responseSms, setResponseSms] = useState({});

    const linkAuth = '/api/users/authorization'
    const linkVerify = '/api/users/sms_verify';
    const linkCheckphone = '/api/users/check_phone'
    const navigate = useNavigate();
    const handleSelectCountry = (country) => {
        setSelectedCountry(country);
    }
    const handleModalShow = () => {
        setShowModal(true);
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        setLoading(true);
      
        const formattedPhoneNumber = phoneNumber.replace(/\D/g, '');
        const finalNumber = `7${formattedPhoneNumber}`;
        const jsonData = {
            phoneNumber: finalNumber,
            option: "authorization"
        }
        setFinalNumber(finalNumber);
        const response = await fetch(linkCheckphone, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify(jsonData)
          });
        const data = await response.json();
    
        if (response.ok) {
          VerifySms()
        }else{
          const error = data.error;
          setErrorCheckPhone(error)
          setLoading(false);
        }
    }
    const VerifySms = async () =>{
        console.log(finalNumber)
        const jsonData = {
            phoneNumber: finalNumber
        }
        try {
            const response = await Verify(jsonData, linkVerify);
          
            if (response.hasOwnProperty('error')) {
              setError(response.error);
              console.log(response.error);
            } else {
              const successResponse = response.sms_code;
              handleModalShow()
              setResponseSms(successResponse)
              console.log("1")
            }
          
          } catch (error) {
            setError("Произошла ошибка при отправке формы.");
            console.error(error);
          } finally {
            setLoading(false);
          }
      };
    const handleModalClose = () => {
        setShowModal(false);
        setSmsCode('');
    };

    const handleModalSubmit = async () => {
        setLoading(true);
        const jsonData = {
            sms_user: smsCode
        }
        try {
        const response = await Verify(jsonData, linkVerify);

        if (response.hasOwnProperty('error')) {
            setError(response.error);
            console.log(response.error);
          } else {
            const successResponse = response.secret;
            setShowModal(false);
            setSmsCode('');
            ContinueUser(successResponse);
        }
        } catch (error) {
            setError("Произошла ошибка при отправке формы.");
            console.error(error);
        } finally {
            setLoading(false);
        }
    };
    const ContinueUser = async (successResponse) => {
        const jsonData = {
            secret: successResponse,
            phoneNumber: finalNumber,
        };

        try {
            const response = await Verify(jsonData, linkAuth);

            if (response.hasOwnProperty('error')) {
                setError(response.error);
                console.log(response.error);
            } else {
                const successResponseId = response.id_user;
                const susccessResponseToken = response.access_token
                localStorage.setItem('access_token', susccessResponseToken);
                navigate('/profile')
            }
        } catch (error) {
            setError("Произошла ошибка при отправке формы.");
            console.error(error);
        } finally {
            setLoading(false);
        }
    };
    return (
        <>
            <Container>
                <Form onSubmit={handleSubmit}>
                    <Row>
                    <Form.Group controlId="formBasicEmail" className={`offset-xxl-4 col-xxl-3 ${styles.border} d-flex`}>
                        <Select selected="RU" defaultCountry="RU" onSelect={handleSelectCountry} customLabels={{ RU: "7" }} className={`w-10 justify-content-start ${styles.select_custom} d-flex`} selectButtonClassName={`justify-content-end ${styles.select_btn_custom}`} />
                        <InputMask mask="(999) 999-99-99" placeholder="Введите номер" inputRef={phoneNumberRef} onChange={(e) => setPhoneNumber(e.target.value)} className={`form-control ${styles.form_control_custom}`} />
                    </Form.Group>
                    <Col xxl="2"><button type="submit" disabled={loading} className='btn btn-custom'>{loading ? "Отправка..." : "Отправить"}</button></Col>
                    </Row>
                    {errorCheckPhone && <p>{errorCheckPhone}</p>}
                </Form>
            </Container>
            {error && <p>{error}</p>}
            {/* {showModal && <ModalSmsVerify responseSms={responseSms }/>} */}
            <Modal show={showModal} onHide={handleModalClose}>
                <Modal.Header closeButton>
                <Modal.Title>Введите код SMS {responseSms}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                <Form.Control
                    type="text"
                    placeholder="Введите код SMS"
                    value={smsCode}
                    onChange={(e) => setSmsCode(e.target.value)}
                />
                </Modal.Body>
                <Modal.Footer>
                <Button variant="secondary" onClick={handleModalClose}>
                Закрыть
            </Button>
            <Button variant="primary" onClick={handleModalSubmit}>
                Отправить
            </Button>
            </Modal.Footer>
        </Modal>
        <Row>
            <Col xxl="3 offset-xxl-4">
                <Link to="/registration" className='nav-link'>Зарегистрироваться</Link>
            </Col>
        </Row>
    </>
    ); };

export default PhoneNumberForm;
