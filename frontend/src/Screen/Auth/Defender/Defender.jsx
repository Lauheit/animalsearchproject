import Footer from "./components/Footer"
import Defender_block from "./components/Defender_block"
import Header from "./components/Header"

const Auth = () => {
    return (
        <>
            <Header />
            <Defender_block />
            <Footer />
        </>
    )
}

export default Auth