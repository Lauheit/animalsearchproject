const AboutMe = () => {
    return(
        <section className="aboutme" id="aboutme">
            <div className="container">
                <div className="row">
                    <div className="col-xxl-10 text-center offset-xxl-1">
                        <div className="offer-aboutme">
                            <h1 className="title">Pet Search services</h1>
                            <p className="content">Pet search services - команда преданных энтузиастов, которые посвятили время и усилия созданию удобного и эффективного инструмента поиска пропавших питомцев.</p>
                            <p className="content">Мы полностью осознаем, насколько тяжело и страшно может быть потерять своё любимое животное. Поэтому наша главная цель - помочь вам преодолеть эту неприятную ситуацию и вернуть вашего питомца домой.</p></div>
                    </div>
                </div>
                <div className="aboutme-number">
                    <div className="row">
                        <div className="col-xxl-3 border-right">
                            <h1 className="title">3000</h1>
                            <p className="content">потерянных питомцев найдено</p>
                            <div className="line"></div>
                        </div>
                        <div className="col-xxl-3 border-right">
                            <h1 className="title">65</h1>
                            <p className="content">регионам предоставляются услуги 
                                Pet Search Services</p>
                            <div className="line"></div>
                        </div>
                        <div className="col-xxl-3 border-right">
                            <h1 className="title">10</h1>
                            <p className="content">лет Pet Search Services успешно предоставляет услуги</p>
                            <div className="line"></div>
                        </div>
                        <div className="col-xxl-3">
                            <h1 className="title">100</h1>
                            <p className="content">сотрудников трудоустроены в Pet Search Services</p>
                            <div className="line"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default AboutMe