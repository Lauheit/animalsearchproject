import SwiperRewiews from "./ui/SwiperRewiews"

const Rewiews = () => {
    return(
        <section className="rewiews">
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-xxl">
                        <div className="offer-rewiews">
                            <div className="title text-center">Что о нас говорят?</div>
                        </div>
                    </div>
                </div>
                <SwiperRewiews />
                <div className="row justify-content-center">
                    <div className="col-xxl-2">
                        <a className="btn btn-custom btn-text btn-size">Все отзывы</a>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Rewiews