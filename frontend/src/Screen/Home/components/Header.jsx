import { Link } from "react-router-dom"
import SwiperHeader from "./ui/SwiperHeader"
import { goNext, goPrev } from "./ui/modules/Button"
import Header_Nav from "../../../componetns/ui/Header"

const Header = () => {
    
    return(
        <header className="header" id="header">
            <div className="container">
                <Header_Nav />
            </div>
            <div className="container">
                <div className="header-main">
                    <div className="row flex-nowrap height">
                        <div className="col-xxl-4">
                            <div className="header-offer">
                                <h1 className="title">Ваш питомец потерялся?</h1>
                                <p className="content">Поможем найти!</p>
                                <Link to={"/offer"} className="btn btn-custom btn-text btn-size">начать поиск</Link>
                            </div>
                        </div>
                        <SwiperHeader />
                    </div>
                    <div className="row">
                        <div className="col-xxl-3 offset-xxl-5">
                            <div className="arrows-slide d-flex justify-content-center">
                                <div className="l-arrow arrow" onClick={goPrev}></div>
                                <div className="r-arrow arrow" onClick={goNext}></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    )
}

export default Header