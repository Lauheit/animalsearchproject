import { useRef } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation, } from 'swiper/modules';
import './modules/style.css'

// Установка модулей Swiper

const SwiperHeader = () => {
    const swiperRef = useRef(null);

    return (
        <Swiper 
            speed={2000}
            ref={swiperRef}
            navigation={{
            prevEl: ".l-arrow",
            nextEl: ".r-arrow",
            }}
            className="col-xxl-7 offset-xxl-1" 
            modules={[Navigation,]} 
            loop={true} 
            spaceBetween={24} 
            slidesPerView={3}
        >
            <div className="swiper-wrapper align-items-center">
                <SwiperSlide >
                    <img src="./src/assets/images/dist/image-2.png" alt="" className="slide-img" />
                </SwiperSlide>
                <SwiperSlide>
                    <img src="./src/assets/images/dist/image-1.png" alt="" className="slide-img" />
                </SwiperSlide>
                <SwiperSlide>
                    <img src="./src/assets/images/dist/image.png" alt="" className="slide-img" />
                </SwiperSlide>
                <SwiperSlide>
                    <img src="./src/assets/images/dist/image-3.png" alt="" className="slide-img" />
                </SwiperSlide>
                <SwiperSlide>
                    <img src="./src/assets/images/dist/image-2.png" alt="" className="slide-img" />
                </SwiperSlide>
                <SwiperSlide>
                    <img src="./src/assets/images/dist/image-1.png" alt="" className="slide-img" />
                </SwiperSlide>
                <SwiperSlide>
                    <img src="./src/assets/images/dist/image.png" alt="" className="slide-img" />
                </SwiperSlide>
                <SwiperSlide>
                    <img src="./src/assets/images/dist/image-3.png" alt="" className="slide-img" />
                </SwiperSlide>
                <SwiperSlide>
                    <img src="./src/assets/images/dist/image-2.png" alt="" className="slide-img" />
                </SwiperSlide>
                <SwiperSlide>
                    <img src="./src/assets/images/dist/image-1.png" alt="" className="slide-img" />
                </SwiperSlide>
                <SwiperSlide>
                    <img src="./src/assets/images/dist/image.png" alt="" className="slide-img" />
                </SwiperSlide>
                <SwiperSlide>
                    <img src="./src/assets/images/dist/image-3.png" alt="" className="slide-img" />
                </SwiperSlide>
            </div>
        </Swiper>
    );
};

export default SwiperHeader;