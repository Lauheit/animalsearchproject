import { useRef } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation, } from 'swiper/modules';
import './modules/style.css'

// Установка модулей Swiper

const SwiperRewiews = () => {
    const swiperRef = useRef(null);

    return (
        <Swiper 
        speed={2400}
        className="rewiews__user"
        ref={swiperRef}
        navigation={{
          prevEl: ".l-arrow",
          nextEl: ".r-arrow",
        }}
        modules={[Navigation,]} 
        loop={true} 
        spaceBetween={24} 
        slidesPerView={1}
        >
            <div className="swiper-wrapper">
                <SwiperSlide >
                    <div className="row align-items-end justify-content-center">
                        <div className="col-xxl-2 d-flex justify-content-center">
                            <div className="l-arrow rewiews-arrow"></div>
                        </div>
                        <div className="col-xxl-2">
                            <div className="image-block">
                                <img src="./src/assets/images/dist/user-avatar.png" alt="" className="image" />
                            </div>
                        </div>
                        <div className="col-xxl-2 d-flex justify-content-center">
                            <div className="r-arrow rewiews-arrow"></div>
                        </div>
                    </div>
                    <div className="row justify-content-center">
                        <div className="col-xxl-2">
                            <h1 className="user__name text-center">Михаил1</h1>
                        </div>
                    </div>
                    <div className="rewiews__text">
                        <div className="row justify-content-center">
                            <div className="col-xxl-8">
                                <p className="content text-center">Параграф (от греч. παράγραφος «написанное рядом») — часть текста внутри раздела книги, статьи или документа, содержащая законченное положение или правило и имеющая самостоятельное значение. Параграф (от греч. παράγραφος «написанное Параграф (от греч. παράγραφος «написанное рядом») — часть текста внутри раздела книги, статьи или документа, содержащая законченное положение или правило и имеющая самостоятельное значение. Параграф (от греч. παράγραφος «написанное</p>
                                <a href="#" className=" text-center nav-link">Посмотреть оригинал</a>
                            </div>
                        </div>
                    </div>
                </SwiperSlide>
                <SwiperSlide >
                    <div className="row align-items-end justify-content-center">
                        <div className="col-xxl-2 d-flex justify-content-center">
                            <div className="l-arrow rewiews-arrow"></div>
                        </div>
                        <div className="col-xxl-2">
                            <div className="image-block">
                                <img src="./src/assets/images/dist/user-avatar.png" alt="" className="image" />
                            </div>
                        </div>
                        <div className="col-xxl-2 d-flex justify-content-center">
                            <div className="r-arrow rewiews-arrow"></div>
                        </div>
                    </div>
                    <div className="row justify-content-center">
                        <div className="col-xxl-2">
                            <h1 className="user__name text-center">Михаил2</h1>
                        </div>
                    </div>
                    <div className="rewiews__text">
                        <div className="row justify-content-center">
                            <div className="col-xxl-8">
                                <p className="content text-center">Параграф (от греч. παράγραφος «написанное рядом») — часть текста внутри раздела книги, статьи или документа, содержащая законченное положение или правило и имеющая самостоятельное значение. Параграф (от греч. παράγραφος «написанное Параграф (от греч. παράγραφος «написанное рядом») — часть текста внутри раздела книги, статьи или документа, содержащая законченное положение или правило и имеющая самостоятельное значение. Параграф (от греч. παράγραφος «написанное</p>
                                <a href="#" className=" text-center nav-link">Посмотреть оригинал</a>
                            </div>
                        </div>
                    </div>
                </SwiperSlide>
                <SwiperSlide >
                    <div className="row align-items-end justify-content-center">
                        <div className="col-xxl-2 d-flex justify-content-center">
                            <div className="l-arrow rewiews-arrow"></div>
                        </div>
                        <div className="col-xxl-2">
                            <div className="image-block">
                                <img src="./src/assets/images/dist/user-avatar.png" alt="" className="image" />
                            </div>
                        </div>
                        <div className="col-xxl-2 d-flex justify-content-center">
                            <div className="r-arrow rewiews-arrow"></div>
                        </div>
                    </div>
                    <div className="row justify-content-center">
                        <div className="col-xxl-2">
                            <h1 className="user__name text-center">Михаил3</h1>
                        </div>
                    </div>
                    <div className="rewiews__text">
                        <div className="row justify-content-center">
                            <div className="col-xxl-8">
                                <p className="content text-center">Параграф (от греч. παράγραφος «написанное рядом») — часть текста внутри раздела книги, статьи или документа, содержащая законченное положение или правило и имеющая самостоятельное значение. Параграф (от греч. παράγραφος «написанное Параграф (от греч. παράγραφος «написанное рядом») — часть текста внутри раздела книги, статьи или документа, содержащая законченное положение или правило и имеющая самостоятельное значение. Параграф (от греч. παράγραφος «написанное</p>
                                <a href="#" className=" text-center nav-link">Посмотреть оригинал</a>
                            </div>
                        </div>
                    </div>
                </SwiperSlide>
                <SwiperSlide >
                    <div className="row align-items-end justify-content-center">
                        <div className="col-xxl-2 d-flex justify-content-center">
                            <div className="l-arrow rewiews-arrow"></div>
                        </div>
                        <div className="col-xxl-2">
                            <div className="image-block">
                                <img src="./src/assets/images/dist/user-avatar.png" alt="" className="image" />
                            </div>
                        </div>
                        <div className="col-xxl-2 d-flex justify-content-center">
                            <div className="r-arrow rewiews-arrow"></div>
                        </div>
                    </div>
                    <div className="row justify-content-center">
                        <div className="col-xxl-2">
                            <h1 className="user__name text-center">Михаил4</h1>
                        </div>
                    </div>
                    <div className="rewiews__text">
                        <div className="row justify-content-center">
                            <div className="col-xxl-8">
                                <p className="content text-center">Параграф (от греч. παράγραφος «написанное рядом») — часть текста внутри раздела книги, статьи или документа, содержащая законченное положение или правило и имеющая самостоятельное значение. Параграф (от греч. παράγραφος «написанное Параграф (от греч. παράγραφος «написанное рядом») — часть текста внутри раздела книги, статьи или документа, содержащая законченное положение или правило и имеющая самостоятельное значение. Параграф (от греч. παράγραφος «написанное</p>
                                <a href="#" className=" text-center nav-link">Посмотреть оригинал</a>
                            </div>
                        </div>
                    </div>
                </SwiperSlide>
                <SwiperSlide >
                    <div className="row align-items-end justify-content-center">
                        <div className="col-xxl-2 d-flex justify-content-center">
                            <div className="l-arrow rewiews-arrow"></div>
                        </div>
                        <div className="col-xxl-2">
                            <div className="image-block">
                                <img src="./src/assets/images/dist/user-avatar.png" alt="" className="image" />
                            </div>
                        </div>
                        <div className="col-xxl-2 d-flex justify-content-center">
                            <div className="r-arrow rewiews-arrow"></div>
                        </div>
                    </div>
                    <div className="row justify-content-center">
                        <div className="col-xxl-2">
                            <h1 className="user__name text-center">Михаил5</h1>
                        </div>
                    </div>
                    <div className="rewiews__text">
                        <div className="row justify-content-center">
                            <div className="col-xxl-8">
                                <p className="content text-center">Параграф (от греч. παράγραφος «написанное рядом») — часть текста внутри раздела книги, статьи или документа, содержащая законченное положение или правило и имеющая самостоятельное значение. Параграф (от греч. παράγραφος «написанное Параграф (от греч. παράγραφος «написанное рядом») — часть текста внутри раздела книги, статьи или документа, содержащая законченное положение или правило и имеющая самостоятельное значение. Параграф (от греч. παράγραφος «написанное</p>
                                <a href="#" className=" text-center nav-link">Посмотреть оригинал</a>
                            </div>
                        </div>
                    </div>
                </SwiperSlide>
                <SwiperSlide >
                    <div className="row align-items-end justify-content-center">
                        <div className="col-xxl-2 d-flex justify-content-center">
                            <div className="l-arrow rewiews-arrow"></div>
                        </div>
                        <div className="col-xxl-2">
                            <div className="image-block">
                                <img src="./src/assets/images/dist/user-avatar.png" alt="" className="image" />
                            </div>
                        </div>
                        <div className="col-xxl-2 d-flex justify-content-center">
                            <div className="r-arrow rewiews-arrow"></div>
                        </div>
                    </div>
                    <div className="row justify-content-center">
                        <div className="col-xxl-2">
                            <h1 className="user__name text-center">Михаил6</h1>
                        </div>
                    </div>
                    <div className="rewiews__text">
                        <div className="row justify-content-center">
                            <div className="col-xxl-8">
                                <p className="content text-center">Параграф (от греч. παράγραφος «написанное рядом») — часть текста внутри раздела книги, статьи или документа, содержащая законченное положение или правило и имеющая самостоятельное значение. Параграф (от греч. παράγραφος «написанное Параграф (от греч. παράγραφος «написанное рядом») — часть текста внутри раздела книги, статьи или документа, содержащая законченное положение или правило и имеющая самостоятельное значение. Параграф (от греч. παράγραφος «написанное</p>
                                <a href="#" className=" text-center nav-link">Посмотреть оригинал</a>
                            </div>
                        </div>
                    </div>
                </SwiperSlide>
            </div>
        </Swiper>
    );
};

export default SwiperRewiews;