const EasySteps = () =>{
    return (
        <section className="EasySteps" id="easysteps">
            <div className="container">
                <div className="offer">
                    <div className="row">
                        <div className="offset-xxl-6 col-xxl-5">
                            <h1 className="title text-end">4 простых шага</h1>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xxl-3 even-element">
                        <div className="step-item">
                            <h1 className="number text-center">01</h1>
                            <div className="image-block">
                                <img src="./src/assets/images/dist/step-1.png" alt="" className="image" />
                            </div>
                            <h1 className="title">Регистрация</h1>
                            <p className="content">Быстрый вход или регистрация по номеру телефона</p>
                        </div>
                    </div>
                    <div className="col-xxl-3 even-element">
                        <div className="step-item">
                            <h1 className="number text-center">02</h1>
                            <div className="image-block">
                                <img src="./src/assets/images/dist/step-2.png" alt="" className="image" />
                            </div>
                            <h1 className="title">Объявление</h1>
                            <p className="content">Простая форма для описания животного</p>
                        </div>
                    </div>
                    <div className="col-xxl-3 even-element">
                        <div className="step-item">
                            <h1 className="number text-center">03</h1>
                            <div className="image-block">
                                <img src="./src/assets/images/dist/step-3.png" alt="" className="image" />
                            </div>
                            <h1 className="title">Поиск</h1>
                            <p className="content">Волонтеры начнут поиски и по завершении свяжутся с клиентом</p>
                        </div>
                    </div>
                    <div className="col-xxl-3 even-element">
                        <div className="step-item">
                            <h1 className="number text-center">04</h1>
                            <div className="image-block">
                                <img src="./src/assets/images/dist/step-4.png" alt="" className="image" />
                            </div>
                            <h1 className="title">Передача</h1>
                            <p className="content">Быстрая и удобная доставка к клиенту пропавшего питомца</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default EasySteps