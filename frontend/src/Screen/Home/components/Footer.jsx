const Footer = () => {
    return(
        <footer className="footer" id="footer">
            <div className="container">
                <div className="row">
                    <nav className="navbar navbar-expand-lg">
                        <div className="col-xxl-3 d-flex">
                            <a className="navbar-brand" href="#header"><div className="logotip"></div></a>
                        </div>
                        <div className="col-xxl-2">
                            <div className="icon-social d-flex">
                                <a href="" className="icon-item telegram"></a>
                                <a href="" className="icon-item vk"></a>
                                <a href="" className="icon-item tenchat"></a>
                            </div>
                        </div>
                        <div className="col-xxl-4 offset-xxl-3">
                            <ul className="navbar-nav justify-content-end">
                                <li className="nav-item">
                                    <a className="nav-link" aria-current="page" href="#easysteps">Помощь</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" aria-current="page" href="#aboutme">О нас</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" aria-current="page" href="#footer">Отзывы</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </footer>
    )
}

export default Footer