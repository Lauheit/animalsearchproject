import EasySteps from './components/EasySteps.jsx'
import AboutMe from './components/AboutMe.jsx'
import Header from './components/Header.jsx'
import Rewiews from './components/Rewiews.jsx'
//import Header from '../Profile/components/Header.jsx'
import Footer from './components/Footer.jsx'

const Home = () => {
    return (
        <>
            <Header />
            <AboutMe />
            <EasySteps />
            <Rewiews />
            <Footer />
        </>
    )
}

export default Home