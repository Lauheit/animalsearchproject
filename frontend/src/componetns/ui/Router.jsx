import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Home from '../../Screen/Home/Home';
import Auth from '../../Screen/Auth/Auth';
import Profile from '../../Screen/Profile/Profile';
import Offer from '../../Screen/Offer/Offer';
import FindList from '../../Screen/FindList/FindList';
import Registration from '../../Screen/Registration/Registration';
import WorkList from '../../Screen/FindList/WorkList/WorkList';

const Router = () => {

    return (
        <BrowserRouter>
            <Routes>
                <Route element={<Home />} path="/" />
                <Route element={<Auth />} path="/auth" />
                <Route element={<Profile />} path="/profile" />
                <Route element={<Offer />} path="/offer" />
                <Route element={<FindList />} path="/findlist" />
                <Route element={<Registration />} path='/registration' />
                <Route element={<WorkList />} path='/worklist' />
                <Route path="*" element={<div>Not Found</div>} />
            </Routes>
        </BrowserRouter>
    )
}

export default Router;