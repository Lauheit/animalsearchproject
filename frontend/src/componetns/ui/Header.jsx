import { Link } from "react-router-dom"

const Header_Nav = () => {
    const localstorage = localStorage.getItem('access_token')

    return (
        <>
            {!localstorage ? (
                <nav className="navbar navbar-expand-lg font-size">
                    <div className="col-xxl-3 d-flex">
                        <Link to={"/"} className="navbar-brand"><div className="logotip"></div></Link>
                    </div>
                    <div className="col-xxl-4">
                        <div className="collapse navbar-collapse text-uppercase" id="navbarSupportedContent">
                            <ul className="navbar-nav">
                                <li className="nav-item">
                                    <Link to={"/#easysteps"} className="nav-link">Помощь</Link>
                                </li>
                                <li className="nav-item">
                                    <Link to={"/#aboutme"} className="nav-link">О нас</Link>
                                </li>
                                <li className="nav-item">
                                    <Link to={"/#footer"} className="nav-link">Отзывы</Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="col-xxl-3 d-flex offset-2 text-uppercase justify-content-end text-end">
                        <Link to="/auth" className="nav-link">Войти</Link>
                    </div>
                </nav>
            ) : (
                <nav className="navbar navbar-expand-lg font-size">
                <div className="col-xxl-3 d-flex">
                    <Link to={"/"} className="navbar-brand"><div className="logotip"></div></Link>
                </div>
                <div className="col-xxl-4">
                    <div className="collapse navbar-collapse text-uppercase" id="navbarSupportedContent">
                        <ul className="navbar-nav">
                            <li className="nav-item">
                                <Link to={"/#easysteps"} className="nav-link">Помощь</Link>
                            </li>
                            <li className="nav-item">
                                <Link to={"/#aboutme"} className="nav-link">О нас</Link>
                            </li>
                            <li className="nav-item">
                                <Link to={"/#footer"} className="nav-link">Отзывы</Link>
                            </li>
                        </ul>
                    </div>
                </div>
                <div className="col-xxl-3 d-flex offset-2 text-uppercase justify-content-end text-end">
                    <Link to="/profile" className="nav-link">Профиль</Link>
                </div>
            </nav>
            )}
        </>
    )
}

export default Header_Nav
