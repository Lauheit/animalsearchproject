import { useState } from "react";
import { Form, Modal, Button } from "react-bootstrap";

const ModalSmsVerify = ({ responseSms }) => {
    const [showModal, setShowModal] = useState(false);
    const [smsCode, setSmsCode] = useState('');
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState('');

    const handleModalClose = () => {
        setShowModal(false);
        setSmsCode('');
    };

    const handleModalSubmit = async () => {
        setLoading(true);
        const jsonData = {
            sms_user: smsCode
        };
        try {
            const response = await Verify(jsonData, linkVerify);

            if (response.hasOwnProperty('error')) {
                setError(response.error);
                console.log(response.error);
            } else {
                const successResponse = response.secret;
                setShowModal(false);
                setSmsCode('');
                ContinueUser(successResponse);
            }
        } catch (error) {
            setError("Произошла ошибка при отправке формы.");
            console.error(error);
        } finally {
            setLoading(false);
        }
    };

    return (
        <Modal show={showModal} onHide={handleModalClose}>
            <Modal.Header closeButton>
                <Modal.Title>Введите код SMS {responseSms}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form.Control
                    type="text"
                    placeholder="Введите код SMS"
                    value={smsCode}
                    onChange={(e) => setSmsCode(e.target.value)}
                />
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={handleModalClose}>
                    Закрыть
                </Button>
                <Button variant="primary" onClick={handleModalSubmit}>
                    {loading ? 'Отправка...' : 'Отправить'}
                </Button>
            </Modal.Footer>
        </Modal>
    );
};

export default ModalSmsVerify;
