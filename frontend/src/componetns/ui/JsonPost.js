const sendDataToServer = async (jsonData, endpoint) => {
  try {
    const response = await fetch(endpoint, {
      method: 'POST',
      body: JSON.stringify(jsonData),
      headers: {
        'Content-Type': 'application/json'
      }
    });

    if (!response.ok) {
      throw new Error('Произошла ошибка при обработке запроса.');
    }

    const data = await response.json();
    return data;
  } catch (error) {
    console.error('Ошибка:', error);
    return { error: 'Произошла ошибка при обработке запроса.' };
  }
};

export default sendDataToServer;