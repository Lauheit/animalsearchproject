const receiveDataToServer = async (endpoint) => {
    try { const response = await fetch(endpoint);
        if (!response.ok) {
          throw new Error('Произошла ошибка при обработке запроса.');
        }
        
        const data = await response.json();
        return data;
    } catch (error) {
        console.error('Ошибка:', error);
        return { error: 'Произошла ошибка при обработке запроса.' };
    }
};

export default receiveDataToServer;