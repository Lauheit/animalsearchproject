const Json = async(jsonData, endpoint) => {
    try {
        const response = await fetch(endpoint, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(jsonData)
        });
        
        const data = await response.json();

        if (response.ok) {
          console.log(data);
          return data
        } else {
          const error = data.error;
          console.log(error);
          return error
        }
      } catch (error) {
        // Обработка сетевой ошибки
        console.error(error);
    }
}

export default Json