import React from 'react';

const VerificationContext = React.createContext();

export default VerificationContext;