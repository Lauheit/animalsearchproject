import sendDataToServer from "./ui/JsonPost";

export const Verify = async (jsonData, linkVerefy) => {
  const data = await sendDataToServer(jsonData, linkVerefy);

  if (data.error) {
    throw new Error(data.error); // Бросить исключение с ошибкой
  } else {
    return data ;
  }
};