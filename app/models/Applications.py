from app import db
from sqlalchemy import text
import sys

class App(db.Model):
    __tablename__ = 'app'
    id = db.Column(db.Integer, primary_key=True)
    animal = db.Column(db.String(20))
    photo = db.Column(db.Text)
    description = db.Column(db.Text)
    # date = db.Column(db.Date)
    status = db.Column(db.String(8))
    circ = db.Column(db.Text)
    animal_name = db.Column(db.Text)
    # loc = db.Column(db.Text)
    tel = db.Column(db.String(11))
    id_user = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=True)

def at_work(id_user,status,id_app):
    try:
        query = text("UPDATE app SET status = :status , id_user = :id_user WHERE id = :id_app;")
        query = query.bindparams(id_app=id_app, status=status, id_user=id_user)
        db.session.execute(query)
        db.session.commit()
        print("OK", file=sys.stderr)
    except Exception as e:
        print(e)

def add_app(data):
    app = App(**data)
    db.session.add(app)
    db.session.commit()

def all_app():
    query = text("SELECT * FROM app WHERE status = 'Новое'")
    result = db.session.execute(query)

    return result

def user_app(status, id_user):
    query = text("SELECT * FROM app WHERE status = :status AND id_user = :id_user;")
    query = query.bindparams(status=status, id_user=id_user)
    result = db.session.execute(query)

    return result