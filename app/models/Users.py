from app import db
import random
from flask_jwt_extended import create_access_token
from datetime import timedelta, datetime
import hashlib, sys
from sqlalchemy import text

user_achievements = db.Table('user_achievements',
    db.Column('user_id', db.Integer, db.ForeignKey('users.id'), primary_key=True),
    db.Column('achievement_id', db.Integer, db.ForeignKey('achievements.id'), primary_key=True)
)

class Users(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    phone = db.Column(db.String(11))
    name = db.Column(db.Text)
    about = db.Column(db.Text)
    photo = db.Column(db.Text)
    birthdate = db.Column(db.Date)
    # sms_code = db.Column(db.String(6))
    achievements = db.relationship('Achievements', secondary=user_achievements, backref='users')
    application = db.relationship('App', backref='users')

    def get_token(self, expire_time=24):
        expire_delta = timedelta(expire_time)
        token = create_access_token(identity=self.id, expires_delta=expire_delta)
        return token

class Achievements(db.Model):
    __tablename__ = 'achievements'
    id = db.Column(db.Integer, primary_key=True)
    photo = db.Column(db.Text)
    name = db.Column(db.Text)

def is_User(phone):
    query = text("SELECT * FROM users WHERE phone = :phone;")
    query = query.bindparams(phone=phone)
    result = db.session.execute(query).fetchone()
    if result is None:
        return False
    else:
        return result[0]

def get_User(id):
    query = text("SELECT * FROM users WHERE id = :id;")
    query = query.bindparams(id=id)
    result = db.session.execute(query).fetchone()
    return result

def generate_sms_code():
    code = ""
    for _ in range(6):
        code += str(random.randint(0, 9))
    return code

def registration_users(data, user, phone):
    user.phone = phone
    user.name = data.get("name")
    user.photo = data.get("photo")
    user.about = data.get("about")
    birthdate = datetime.strptime(data.get("birthdate"), "%Y-%m-%d").date()
    user.birthdate = birthdate

    db.session.add(user)
    db.session.commit()


def is_verify(sms, phone=None, check=False):
    hesh = str(sms) + str(phone)
    print(sms, phone, file=sys.stderr)
    hesh = hashlib.md5(hesh.encode()).hexdigest()
    print(hesh, file=sys.stderr)
    if check is False:
        return hesh
    elif hesh == check: return True
    else: return False
