from flask import Blueprint, request, jsonify
from models.Applications import at_work, add_app, all_app, user_app
from flask_jwt_extended import jwt_required, get_jwt_identity

app_controller = Blueprint('app_controller', __name__, url_prefix='/api/applications')

@app_controller.route('/add',  methods=['POST'])
def add():
    if request.method == 'POST':
        req_data = request.get_json()
        add_app(req_data)

        return {"msg" : "Заявка добавлена"}, 200

@app_controller.route('/get_all_app',  methods=['GET'])
def get_all_app():
    if request.method == 'GET':
        result = all_app()
        app = [dict(row._asdict()) for row in result]

        return jsonify(app), 200

@app_controller.route('/edit',  methods=['POST'])
@jwt_required()
def edit():
    if request.method == 'POST':
        id_user = get_jwt_identity()
        req_data = request.get_json()
        id_app = int(req_data.get("id_app"))
        status = req_data.get("status")

        at_work(id_user,status,id_app)
        
        return {"msg" : f"Статус заявки был изменен на '{status}'"}, 200
    
@app_controller.route('/get_user_app',  methods=['POST'])
@jwt_required()
def get_user_app():
    if request.method == 'POST':
        id_user = get_jwt_identity()
        req_data = request.get_json()
        status = req_data.get("status")

        result = user_app(status, id_user)
        res = [dict(row._asdict()) for row in result]
        return jsonify(res), 200