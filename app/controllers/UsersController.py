from flask import Blueprint, request
from models.Users import Users, generate_sms_code, is_User, registration_users, is_verify, get_User
from flask_jwt_extended import jwt_required, get_jwt_identity
from datetime import datetime

users_controller = Blueprint('users_controller', __name__, url_prefix='/api/users')
sms_code = ""
phone = ""

@users_controller.route('/sms_verify',  methods=['POST'])
def sms_verify():
    global sms_code
    global phone
    if request.method == 'POST':
        req_data = request.get_json()
        sms_user = req_data.get("sms_user")
        if phone == "":
            phone = req_data.get("phoneNumber")

        if not len(phone) == 11 or not phone.isdigit():
            return {"error" : "Неверно набран номер"}, 400
        
        if sms_user is None or sms_user == "":
            sms_code = generate_sms_code()
            return {
                "sms_code": sms_code
            }, 200
        elif not sms_user == sms_code:
            return {
                "error": "Invalid SMS code",
                "sms_code": sms_code,
                "sms_user": sms_user
            }, 400
        else:
            secret = is_verify(sms_code, phone)
            phone = ""
            return {"msg" : "Verification was successful", "secret": secret}, 200

@users_controller.route('/check_phone',  methods=['POST'])
def check_phone():
    if request.method == 'POST':
        phone = request.get_json().get("phoneNumber")
        option = request.get_json().get("option")
        if is_User(phone) and option == "registration":
            return {"error" : "Номер уже зарегестрирован"}, 400
        elif is_User(phone) is False and option == "authorization":
            return {"error" : "Пользователя с тамим номером не существует"}, 400
        else:
            return {"msg" : "Проверка прошла успешно"}, 200



@users_controller.route('/authorization',  methods=['POST'])
def authorization():
    global sms_code
    if request.method == 'POST':
        req_data = request.get_json()
        secret = req_data.get("secret")
        phone = req_data.get("phoneNumber")

        if not len(phone) == 11 or not phone.isdigit():
            return {"error" : "Неверно набран номер"}, 400

        if not is_verify(sms_code,phone,secret): 
            return {"error": "Пройдите верификацию номера телефона еще раз"}, 400
       
        if is_User(phone) is False:
            return {"error" : "Пользователя с тамим номером не существует"}, 400
        existing_user = Users.query.filter_by(phone=phone).first()
        return {
            "access_token": existing_user.get_token(),
            "id_user": is_User(phone)
        }, 200

@users_controller.route('/registration',  methods=['POST'])
def registration():
    if request.method == 'POST':
        req_data = request.get_json()
        secret = req_data.get("secret")
        phone = req_data.get("phoneNumber")

        if not len(phone) == 11 or not phone.isdigit():
            return {"error" : "Неверно набран номер"}, 400

        if not is_verify(sms_code,phone,secret): 
            return {"error": "Пройдите верификацию номера телефона еще раз"}, 400
        
        if is_User(phone):
            return {"error" : "Пользователь с таким номером существует"}, 400
        
        user = Users(phone=phone)
        registration_users(req_data, user, phone)

        return {
                "access_token": user.get_token(),
                "id_user": is_User(phone)
            }, 200

@users_controller.route('/profile',  methods=['GET'])
@jwt_required()
def profile():
    if request.method == 'GET':
        id = get_jwt_identity()
        user = get_User(id)
        if user is None:
            return {"error" : "Такого пользователся не существует"}, 400
        return{
            "photo": user[1],
            "name": user[2],
            "about": user[3],
            "photo": user[4],
            "birthdate": datetime.strftime(user[5], ("%Y-%m-%d"))
        }, 200
