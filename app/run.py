from controllers.UsersController import users_controller
from controllers.AppController import app_controller
from app import app
import os

app.register_blueprint(users_controller)
app.register_blueprint(app_controller)

if __name__ == "__main__":
  port = int(os.environ.get("PORT", 5000))
  app.run(debug=True, host="0.0.0.0", port=port)