from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_jwt_extended import JWTManager
from flask_migrate import Migrate

app = Flask(__name__)

app.config["SQLALCHEMY_DATABASE_URI"] = "postgresql://postgres:postgres@db:5432/animalsearchproject"
app.config["SECRET_KEY"] = "lksdjbkj43tkjfdb8i98435mnjoifd"

db = SQLAlchemy(app)
migrate = Migrate(app, db)
jwt = JWTManager(app)