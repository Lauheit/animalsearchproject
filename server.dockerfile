FROM python:3.11

COPY /app /server/app
COPY requirements.txt /server/app
WORKDIR /server/app
RUN pip install -r requirements.txt

ENV FLASK_APP run.py

EXPOSE 5000

CMD ["flask", "run", "--host=0.0.0.0"]

# CMD [ "uwsgi", "app.ini" ]