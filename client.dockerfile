FROM nginx

WORKDIR /usr/share/react

RUN curl -fsSL https://deb.nodesource.com/setup_18.x | bash -
RUN apt-get install -y nodejs

COPY ./frontend/package*.json ./

RUN npm install --legacy-peer-deps
COPY ./frontend .

RUN set PUBLIC_URL=http://localhost

RUN npm run build

RUN rm -r /usr/share/nginx/html/*

RUN cp -a * /usr/share/nginx/html/

RUN mv /usr/share/nginx/html/dist/* /usr/share/nginx/html

COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf

ENTRYPOINT ["nginx", "-g", "daemon off;"]
